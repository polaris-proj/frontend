# Polaris Project
Polaris Project FrontEnd Application



## Building in docker

```bash
docker build . -t frontend
docker run -it -p 80:80 frontend
```

***
## Start in dev
```bash
#Install modules
yarn install
#Start app
yarn start
#Test infra are running
yarn playwright test
```
## Build in prod
```bash
#Install modules
yarn install
#Build app
yarn build-prod
```
## Build in dev
```bash
#Install modules
yarn install
#Build app
yarn build
```
## Lint application
```bash
#Install modules
yarn install
#Build app
yarn lint
```
