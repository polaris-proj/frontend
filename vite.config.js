import { defineConfig } from "vite";
import { ViteAliases } from "vite-aliases"
import react from '@vitejs/plugin-react'

export default defineConfig(
    {
        build: {
            target: '',
            outDir: 'dist',
        },
        server: {
            port: 3001,
            host: 'localhost',
            hmr: true,
            proxy: {
              '/api': {
                target: "http://localhost:7139",
                changeOrigin: true,
                rewrite: (path) => path.replace(/^\/api/, ''),
              },
            },
        },
        css: {
          preprocessorOptions: {
            less: {
              javascriptEnabled: true,
              additionalData: '@root-entry-name: default;',
            }
          }
        },
        plugins:[
            ViteAliases(),
            react({
                babel: {
                    plugins: [['styled-components', { displayName: true }]]
                },
            })
        ],
    }
)
