FROM node:18-alpine as build
WORKDIR /app

COPY package.json /app/package.json
RUN yarn install

COPY . /app
RUN yarn build

FROM node:18-alpine

RUN yarn add express axios http-proxy-middleware
WORKDIR /app
COPY --from=build /app/dist /app/dist
COPY index.js /app/index.js

ENV PORT=80
CMD ["node", "index.js"]
