/* eslint-disable */
// TypeScriptContractGenerator's generated content
import type { StartStrategyModel } from './../DataTypes/StartStrategyModel';
import type { StartTestOfStrategyModel } from './../DataTypes/StartTestOfStrategyModel';
import type { TestOfStrategyViewModel } from './../DataTypes/TestOfStrategyViewModel';
import type { Exchange } from './../DataTypes/Exchange';
import type { TimeFrame } from './../DataTypes/TimeFrame';
import type { ParameterModel } from './../DataTypes/ParameterModel';
import type { TestResult } from './../DataTypes/TestResult';
import type { StrategyUserPresentation } from './../DataTypes/StrategyUserPresentation';
import type { IActionResult } from './../DataTypes/IActionResult';
import type { StrategyUserPresentationEditable } from './../DataTypes/StrategyUserPresentationEditable';
import type { StrategyWithParameters } from './../DataTypes/StrategyWithParameters';
import { ApiBase } from './../ApiBase/ApiBase';

export class StrategyApi extends ApiBase implements IStrategyApi {
    async startStrategy(arg: StartStrategyModel): Promise<string> {
        return this.makePostRequest(`/strategies/start`, {
            
        }, {
            ...arg,
        });
    }

    async startTestOfStrategy(arg: StartTestOfStrategyModel): Promise<string> {
        return this.makePostRequest(`/strategies/startTest`, {
            
        }, {
            ...arg,
        });
    }

    async getResultOfExperiment(runId: string, exchange: Exchange, firstTicker: string, secondTicker: string, timeFrame: TimeFrame, startTime: (Date | string), endTime: (Date | string)): Promise<TestOfStrategyViewModel> {
        return this.makeGetRequest(`/strategies/result/${runId}`, {
            ['exchange']: exchange,
            ['firstTicker']: firstTicker,
            ['secondTicker']: secondTicker,
            ['timeFrame']: timeFrame,
            ['startTime']: startTime,
            ['endTime']: endTime,
        }, {
            
        });
    }

    async getStrategyParameters(strategyId: string): Promise<ParameterModel[]> {
        return this.makeGetRequest(`/strategies/${strategyId}/parameters`, {
            
        }, {
            
        });
    }

    async getTestStrategyParameters(runId: string): Promise<StartTestOfStrategyModel> {
        return this.makeGetRequest(`/strategies/testRun/${runId}`, {
            
        }, {
            
        });
    }

    async getStrategyRealRuns(strategyId: string): Promise<string[]> {
        return this.makeGetRequest(`/strategies/${strategyId}/realRuns`, {
            
        }, {
            
        });
    }

    async getStrategyTestRuns(strategyId: string): Promise<TestResult[]> {
        return this.makeGetRequest(`/strategies/${strategyId}/testRuns`, {
            
        }, {
            
        });
    }

    async getStrategyInfo(strategyId: string): Promise<StrategyUserPresentation> {
        return this.makeGetRequest(`/strategies/${strategyId}/strategyInfo`, {
            
        }, {
            
        });
    }

    async createStrategyInfo(strategyUserPresentationEditable: StrategyUserPresentationEditable): Promise<IActionResult> {
        return this.makePostRequest(`/strategies/${strategyId}/strategyInfo`, {
            
        }, {
            ...strategyUserPresentationEditable,
        });
    }

    async editStrategyInfo(strategyUserPresentationEditable: StrategyUserPresentationEditable): Promise<void> {
        return this.makePutRequest(`/strategies/${strategyId}/strategyInfo`, {
            
        }, {
            ...strategyUserPresentationEditable,
        });
    }

    async getStrategyList(): Promise<StrategyWithParameters[]> {
        return this.makeGetRequest(`/strategies`, {
            
        }, {
            
        });
    }

    async getStrategy(strategyId: string): Promise<StrategyWithParameters> {
        return this.makeGetRequest(`/strategies/${strategyId}`, {
            
        }, {
            
        });
    }

};
export interface IStrategyApi {
    startStrategy(arg: StartStrategyModel): Promise<string>;
    startTestOfStrategy(arg: StartTestOfStrategyModel): Promise<string>;
    getResultOfExperiment(runId: string, exchange: Exchange, firstTicker: string, secondTicker: string, timeFrame: TimeFrame, startTime: (Date | string), endTime: (Date | string)): Promise<TestOfStrategyViewModel>;
    getStrategyParameters(strategyId: string): Promise<ParameterModel[]>;
    getTestStrategyParameters(runId: string): Promise<StartTestOfStrategyModel>;
    getStrategyRealRuns(strategyId: string): Promise<string[]>;
    getStrategyTestRuns(strategyId: string): Promise<TestResult[]>;
    getStrategyInfo(strategyId: string): Promise<StrategyUserPresentation>;
    createStrategyInfo(strategyUserPresentationEditable: StrategyUserPresentationEditable): Promise<IActionResult>;
    editStrategyInfo(strategyUserPresentationEditable: StrategyUserPresentationEditable): Promise<void>;
    getStrategyList(): Promise<StrategyWithParameters[]>;
    getStrategy(strategyId: string): Promise<StrategyWithParameters>;
}
