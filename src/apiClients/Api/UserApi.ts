/* eslint-disable */
// TypeScriptContractGenerator's generated content
import type { ExchangeKeyDto } from './../DataTypes/ExchangeKeyDto';
import type { ExchangeKey } from './../DataTypes/ExchangeKey';
import type { Exchange } from './../DataTypes/Exchange';
import type { User } from './../DataTypes/User';
import { ApiBase } from './../ApiBase/ApiBase';

export class UserApi extends ApiBase implements IUserApi {
    async getExchangeKeys(): Promise<ExchangeKeyDto[]> {
        return this.makeGetRequest(`/user/exchangeKeys`, {
            
        }, {
            
        });
    }

    async addExchangeKey(exchangeKey: ExchangeKey): Promise<void> {
        return this.makePostRequest(`/user/exchangeKeys`, {
            
        }, {
            ...exchangeKey,
        });
    }

    async removeExchangeKey(exchange: Exchange): Promise<void> {
        return this.makeDeleteRequest(`/user/exchangeKeys`, {
            ['exchange']: exchange,
        }, {
            
        });
    }

    async getUserInfo(): Promise<User> {
        return this.makeGetRequest(`/user/info`, {
            
        }, {
            
        });
    }

};
export interface IUserApi {
    getExchangeKeys(): Promise<ExchangeKeyDto[]>;
    addExchangeKey(exchangeKey: ExchangeKey): Promise<void>;
    removeExchangeKey(exchange: Exchange): Promise<void>;
    getUserInfo(): Promise<User>;
}
