/* eslint-disable */
// TypeScriptContractGenerator's generated content
import type { LoginResult } from './../DataTypes/LoginResult';
import type { LoginData } from './../DataTypes/LoginData';
import type { RegisterData } from './../DataTypes/RegisterData';
import { ApiBase } from './../ApiBase/ApiBase';

export class AuthApi extends ApiBase implements IAuthApi {
    async login(loginData: LoginData): Promise<LoginResult> {
        return this.makePostRequest(`/auth/login`, {
            
        }, {
            ...loginData,
        });
    }

    async logout(): Promise<void> {
        return this.makePostRequest(`/auth/logout`, {
            
        }, {
            
        });
    }

    async register(registerData: RegisterData): Promise<void> {
        return this.makePostRequest(`/auth/register`, {
            
        }, {
            ...registerData,
        });
    }

    async refreshToken(refreshToken: string): Promise<LoginResult> {
        return this.makePostRequest(`/auth/refresh-token`, {
            ['refreshToken']: refreshToken,
        }, {
            
        });
    }

};
export interface IAuthApi {
    login(loginData: LoginData): Promise<LoginResult>;
    logout(): Promise<void>;
    register(registerData: RegisterData): Promise<void>;
    refreshToken(refreshToken: string): Promise<LoginResult>;
}
