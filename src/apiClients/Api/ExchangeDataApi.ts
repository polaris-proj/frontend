/* eslint-disable */
// TypeScriptContractGenerator's generated content
import type { Pair } from './../DataTypes/Pair';
import type { Exchange } from './../DataTypes/Exchange';
import type { Candle } from './../DataTypes/Candle';
import type { TimeFrame } from './../DataTypes/TimeFrame';
import { ApiBase } from './../ApiBase/ApiBase';

export class ExchangeDataApi extends ApiBase implements IExchangeDataApi {
    async getPairsForExchange(exchange: Exchange): Promise<Pair[]> {
        return this.makeGetRequest(`/exchange/${exchange}/pairs`, {
            
        }, {
            
        });
    }

    async getPairs(): Promise<{
        [key in Exchange]?: Pair[];
    }> {
        return this.makeGetRequest(`/exchange/pairs`, {
            
        }, {
            
        });
    }

    async getCandles(exchange: Exchange, firstTicker: string, secondTicker: string, timeFrame: TimeFrame, startTime: (Date | string), endTime: (Date | string)): Promise<Candle[]> {
        return this.makeGetRequest(`/exchange/${exchange}/candles`, {
            ['firstTicker']: firstTicker,
            ['secondTicker']: secondTicker,
            ['timeFrame']: timeFrame,
            ['startTime']: startTime,
            ['endTime']: endTime,
        }, {
            
        });
    }

};
export interface IExchangeDataApi {
    getPairsForExchange(exchange: Exchange): Promise<Pair[]>;
    getPairs(): Promise<{
        [key in Exchange]?: Pair[];
    }>;
    getCandles(exchange: Exchange, firstTicker: string, secondTicker: string, timeFrame: TimeFrame, startTime: (Date | string), endTime: (Date | string)): Promise<Candle[]>;
}
