/* eslint-disable */
// TypeScriptContractGenerator's generated content
import type { NoteViewModel } from './NoteViewModel';
import type { OrderViewModel } from './OrderViewModel';
import type { ChartViewModel } from './ChartViewModel';

export type TestOfStrategyViewModel = {
    notes: NoteViewModel[];
    orders: OrderViewModel[];
    charts: ChartViewModel[];
};
