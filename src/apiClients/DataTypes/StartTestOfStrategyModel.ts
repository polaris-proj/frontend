/* eslint-disable */
// TypeScriptContractGenerator's generated content
import type { ParameterModel } from './ParameterModel';
import type { ExchangeSources } from './ExchangeSources';

export type StartTestOfStrategyModel = {
    initialBalance: number;
    isRealTimeTestMode: boolean;
    enableOrderConfirmation: boolean;
    endTime: (Date | string);
    strategyId: string;
    parameters: ParameterModel[];
    allowedSource: ExchangeSources[];
    startTime: (Date | string);
};
