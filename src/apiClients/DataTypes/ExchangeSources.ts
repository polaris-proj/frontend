/* eslint-disable */
// TypeScriptContractGenerator's generated content
import type { Exchange } from './Exchange';
import type { PairTimeFrame } from './PairTimeFrame';

export type ExchangeSources = {
    exchange: Exchange;
    pairTimeFrame: PairTimeFrame[];
};
