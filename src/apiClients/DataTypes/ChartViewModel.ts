/* eslint-disable */
// TypeScriptContractGenerator's generated content
import type { TimeFrame } from './TimeFrame';
import type { Pair } from './Pair';
import type { Point } from './Point';

export type ChartViewModel = {
    name: string;
    description?: null | string;
    timeFrame: TimeFrame;
    pair: Pair;
    chart: Point[];
};
