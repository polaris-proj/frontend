/* eslint-disable */
// TypeScriptContractGenerator's generated content
import type { Description } from './Description';

export type StrategyUserPresentation = {
    id: string;
    name?: null | string;
    descriptions?: null | Description[];
};
