/* eslint-disable */
// TypeScriptContractGenerator's generated content

export enum Exchange {
    BinanceSpot = 'BinanceSpot',
    Poloniex = 'Poloniex',
    BinanceFutures = 'BinanceFutures',
    BinanceSpotTest = 'BinanceSpotTest',
    BinanceFuturesTest = 'BinanceFuturesTest',
}
