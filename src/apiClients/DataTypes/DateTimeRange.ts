/* eslint-disable */
// TypeScriptContractGenerator's generated content

export type DateTimeRange = {
    begin: (Date | string);
    end: (Date | string);
};
