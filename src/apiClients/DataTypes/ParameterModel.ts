/* eslint-disable */
// TypeScriptContractGenerator's generated content
import type { ParameterType } from './ParameterType';

export type ParameterModel = {
    name: string;
    typeOfValue: ParameterType;
    value: string;
};
