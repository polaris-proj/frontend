/* eslint-disable */
// TypeScriptContractGenerator's generated content

export enum OrderType {
    Buy = 'Buy',
    Sell = 'Sell',
    Long = 'Long',
    Short = 'Short',
    BuyLimit = 'BuyLimit',
    SellLimit = 'SellLimit',
}
