/* eslint-disable */
// TypeScriptContractGenerator's generated content
import type { ParameterModel } from './ParameterModel';
import type { ExchangeSources } from './ExchangeSources';

export type StartStrategyModel = {
    strategyId: string;
    parameters: ParameterModel[];
    allowedSource: ExchangeSources[];
    startTime: (Date | string);
};
