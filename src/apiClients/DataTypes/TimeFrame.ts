/* eslint-disable */
// TypeScriptContractGenerator's generated content

export enum TimeFrame {
    Unknown = 'Unknown',
    m1 = 'm1',
    m3 = 'm3',
    m5 = 'm5',
    m15 = 'm15',
    m30 = 'm30',
    h1 = 'h1',
    h4 = 'h4',
    h12 = 'h12',
    D1 = 'D1',
    D3 = 'D3',
    W1 = 'W1',
    Mo1 = 'Mo1',
}
