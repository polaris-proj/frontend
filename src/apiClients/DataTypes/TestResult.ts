/* eslint-disable */
// TypeScriptContractGenerator's generated content
import type { DateTimeRange } from './DateTimeRange';

export type TestResult = {
    runId: string;
    timeOfRun: (Date | string);
    profit: number;
    duration: DateTimeRange;
};
