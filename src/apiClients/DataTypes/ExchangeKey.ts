/* eslint-disable */
// TypeScriptContractGenerator's generated content
import type { Exchange } from './Exchange';

export type ExchangeKey = {
    exchange: Exchange;
    secret: string;
    token: string;
};
