/* eslint-disable */
// TypeScriptContractGenerator's generated content
import type { Exchange } from './Exchange';

export type ExchangeKeyDto = {
    exchange: Exchange;
    key: string;
    secret: string;
};
