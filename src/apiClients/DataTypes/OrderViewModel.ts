/* eslint-disable */
// TypeScriptContractGenerator's generated content
import type { Pair } from './Pair';
import type { OrderType } from './OrderType';
import type { OrderStatus } from './OrderStatus';

export type OrderViewModel = {
    currencyPair: Pair;
    type: OrderType;
    status: OrderStatus;
    price: number;
    amount: number;
    orderId?: null | string;
    createdAt: number;
    updatedAt?: null | (Date | string);
    leverage?: null | number;
    takePrice?: null | number;
    stopPrice?: null | number;
};
