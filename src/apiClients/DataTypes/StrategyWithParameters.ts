/* eslint-disable */
// TypeScriptContractGenerator's generated content
import type { StrategyConnectorType } from './StrategyConnectorType';
import type { ParameterModel } from './ParameterModel';

export type StrategyWithParameters = {
    id: string;
    internalName: string;
    connectorType: StrategyConnectorType;
    properties: ParameterModel[];
};
