/* eslint-disable */
// TypeScriptContractGenerator's generated content

export type LoginResult = {
    login: string;
    roles: string[];
    accessToken: string;
    refreshToken: string;
};
