/* eslint-disable */
// TypeScriptContractGenerator's generated content

export enum OrderStatus {
    Open = 'Open',
    PartiallyFilled = 'PartiallyFilled',
    Close = 'Close',
    Created = 'Created',
}
