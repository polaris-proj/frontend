/* eslint-disable */
// TypeScriptContractGenerator's generated content
import type { Pair } from './Pair';
import type { TimeFrame } from './TimeFrame';

export type PairTimeFrame = {
    pair: Pair;
    timeFrame: TimeFrame;
};
