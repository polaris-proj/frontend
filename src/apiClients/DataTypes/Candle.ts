/* eslint-disable */
// TypeScriptContractGenerator's generated content

export type Candle = {
    timeStamp: number;
    open: number;
    high: number;
    low: number;
    close: number;
    volume: number;
};
