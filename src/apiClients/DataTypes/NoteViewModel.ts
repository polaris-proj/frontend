/* eslint-disable */
// TypeScriptContractGenerator's generated content
import type { Icon } from './Icon';

export type NoteViewModel = {
    timeStamp: number;
    price: number;
    text?: null | string;
    icon: Icon;
};
