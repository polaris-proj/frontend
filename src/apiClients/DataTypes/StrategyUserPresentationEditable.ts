/* eslint-disable */
// TypeScriptContractGenerator's generated content
import type { Description } from './Description';

export type StrategyUserPresentationEditable = {
    id: string;
    name: string;
    descriptions: Description[];
};
