import { type DependencyList, useEffect } from 'react'
import { ExchangeDataApi } from '../Api/ExchangeDataApi'
import { StrategyApi } from '../Api/StrategyApi'
import {UserApi} from "~apiClients/Api/UserApi";
import {AuthApi} from "~apiClients/Api/AuthApi";

export const exchangeDataApi = new ExchangeDataApi()
export const useExchangeDataApi = (callback: (api: ExchangeDataApi) => void, deps?: DependencyList): void => {
  useEffect(() => {
    callback(exchangeDataApi)
  }, deps)
}
export const strategyApi = new StrategyApi()
export const useStrategyApi = (callback: (api: StrategyApi) => void, deps?: DependencyList): void => {
  useEffect(() => {
    callback(strategyApi)
  }, deps)
}

export const userApi = new UserApi()

export const useUserApi = (callback: (api: UserApi) => void, deps?: DependencyList): void => {
  useEffect(() => {
    callback(userApi)
  }, deps)
}

export const authApi = new AuthApi()

export const useAuthApi = (callback: (api: AuthApi) => void, deps?: DependencyList): void => {
  useEffect(() => {
    callback(authApi)
  }, deps)
}
