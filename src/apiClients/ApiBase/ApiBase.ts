import {axiosInstance} from "~/services";

export class ApiBase {
  public async makeGetRequest (url: string, queryParams: Record<string, any>, headers: any): Promise<any> {
    return await axiosInstance.get(url, {
        headers: {
            'Content-Type': 'application/json', // todo точно нужен?
            ...headers
        },
      params: queryParams
    }).then((response) => response.data)
      .catch((error) => {throw error})
  }

  public async makePostRequest (url: string, queryParams: Record<string, any>, body: any): Promise<any> {
    return await axiosInstance.post(url, body,{
      headers: {
        'Content-Type': 'application/json',
      },
        params: queryParams
      }
    ).then((response) => response.data)
      .catch((error) => {throw error});
  }

  public async makePutRequest (url: string, queryParams: Record<string, any>, body: any): Promise<any> {
    return  await axiosInstance.put(url, {
      method: 'PUT',
      params: queryParams,
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    });
  }

  public async makeDeleteRequest (url: string, queryParams: Record<string, any>, headers: any): Promise<any> {
    return  await axiosInstance.delete(url, {
      method: 'DELETE',
      params: queryParams,
      headers: {
        'Content-Type': 'application/json', // todo точно нужен?
        ...headers
      }
    });
  }
}
