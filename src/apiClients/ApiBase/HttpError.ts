export class HttpError extends Error{
  public status: number;
  constructor(status: number, message: string) {
    super(`HTTP error! status: ${status} message: ${message}`);
    this.status = status;
  }
}
