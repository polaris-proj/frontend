import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

// Login page locale
import loginEn from './login/en.json';
import loginRu from './login/ru.json';

// Registration page locale
import registrationEn from './registration/en.json';
import registrationRu from './registration/ru.json';

// Forgot password page locale
import forgotPasswordEn from './forgot-password/en.json';
import forgotPasswordRu from './forgot-password/ru.json';

// Forgot password page locale
import pathsEn from './paths/en.json';
import pathsRu from './paths/ru.json';

// Layout locale
import layoutEn from './layout/en.json';
import layoutRu from './layout/ru.json';

// Charts locale
import chartsEn from './charts/en.json';
import chartsRu from './charts/ru.json';

// Strategy locale
import strategyEn from './strategy/en.json';
import strategyRu from './strategy/ru.json';

import testEn from './test/en.json';
import testRu from './test/ru.json';

import mainEn from './main/en.json';
import mainRu from './main/ru.json';

export enum LocaleKeys {
  LOGIN = 'login',
  REGISTRATION = 'registration',
  FORGOT_PASSWORD = 'forgot-password',
  PATHS = 'paths',
  LAYOUT = 'layout',
  CHARTS = 'charts',
  STRATEGY = 'strategy',
  TEST = 'test',
  MAIN = 'main',
}

i18n
  .use(initReactI18next)
  .init({
    fallbackLng: 'en',
    debug: false,
    react: {
      bindI18n: 'loaded languageChanged',
      bindI18nStore: 'added',
      useSuspense: true,
    },
    resources: {
      en: {
        [LocaleKeys.LOGIN]: loginEn,
        [LocaleKeys.REGISTRATION]: registrationEn,
        [LocaleKeys.FORGOT_PASSWORD]: forgotPasswordEn,
        [LocaleKeys.PATHS]: pathsEn,
        [LocaleKeys.LAYOUT]: layoutEn,
        [LocaleKeys.CHARTS]: chartsEn,
        [LocaleKeys.STRATEGY]: strategyEn,
        [LocaleKeys.TEST]: testEn,
        [LocaleKeys.MAIN]: mainEn,
      },
      ru: {
        [LocaleKeys.LOGIN]: loginRu,
        [LocaleKeys.REGISTRATION]: registrationRu,
        [LocaleKeys.FORGOT_PASSWORD]: forgotPasswordRu,
        [LocaleKeys.PATHS]: pathsRu,
        [LocaleKeys.LAYOUT]: layoutRu,
        [LocaleKeys.CHARTS]: chartsRu,
        [LocaleKeys.STRATEGY]: strategyRu,
        [LocaleKeys.TEST]: testRu,
        [LocaleKeys.MAIN]: mainRu,
      },
    },
    interpolation: {
      escapeValue: false,
    },
  })
  .then(() => {});

export default i18n;
