import React, {FC, useCallback, useMemo, useState} from 'react';
import {DatePicker, Form, Modal, Select} from 'antd';
import {IExchangedParamsItem, TimeFrameList,} from '~ducks/strategies/slices/exchangeParamsSlice';
import styled from '@emotion/styled';
import dayjs, {Dayjs} from 'dayjs';
import isBetween from 'dayjs/plugin/isBetween'
import {Pair} from "~apiClients/DataTypes/Pair";
import {useExchangeDataApi} from "~apiClients/hooks/useApi";
import {Exchange} from "~apiClients/DataTypes/Exchange";
import {TimeFrame} from "~apiClients/DataTypes/TimeFrame";
import type {ParameterModel} from "~apiClients/DataTypes/ParameterModel";
import {ParameterForm} from "~modals/parameters-modal";

dayjs.extend(isBetween)

const { Option } = Select;

const Divider = styled('div')`
  height: 1px;
  background-color: ${({ theme }) => theme.COLORS.WHITE.C400};
  margin: 12px 0;
`;

interface Props {
  isModalOpen: boolean;
  handleStartStrategy: (some: {
    dateStart: Dayjs;
    dateEnd: Dayjs;
    sources: IExchangedParamsItem[];
    parameters: ParameterModel[];
  }) => void;
  handleCloseModal: () => void;
  params: ParameterModel[];
}

interface IForm {
  startDate: Dayjs;
  endDate: Dayjs;
  exchange: number;
  pair: string;
  timeframe: number;
}

export const StartStrategyModal: FC<Props> = ({isModalOpen, handleStartStrategy, handleCloseModal,params}) => {
  const [form] = Form.useForm();

  const [startDate, setStartDate] = useState<Dayjs | undefined>(undefined);
  const [endDate, setEndDate] = useState<Dayjs | undefined>(undefined);
  const [selectedExchangeParams, setSelectedExchangeParams] = useState<
    IExchangedParamsItem[]
  >([]);

  const handleEditSelectedExchangeParams = useCallback((values: Exchange[]) => {
    setSelectedExchangeParams((param) => {
      const filteredResult = param.filter(
        (el) => values.indexOf(el.exchange) > -1
      );

      if (filteredResult.length === values.length) {
        return filteredResult;
      } else {
        values.forEach((value) => {
          if (
            filteredResult.find((el) => el.exchange === value) === undefined
          ) {
            filteredResult.push({
              exchange: value,
              sources: [],
            });
          }
        });
      }

      return filteredResult;
    });
  }, []);

  const handleEditPairs = useCallback(
    (values: string[], exchange: Exchange) => {
      setSelectedExchangeParams((param) =>
        param.map((el) => {
          if (el.exchange === exchange) {
            const pairResult: IExchangedParamsItem['sources'] = [];

            el.sources.forEach((pairEl) => {
              if (values.indexOf(pairEl.pair) > -1) {
                pairResult.push(pairEl);
              }
            });

            if (values.length !== pairResult.length) {
              values.forEach((valueEl) => {
                if (
                  pairResult.find((pairEl) => pairEl.pair === valueEl) ===
                  undefined
                ) {
                  pairResult.push({
                    pair: valueEl,
                    timeFrame: [TimeFrame.Unknown]
                  });
                }
              });
            }

            return {
              ...el,
              sources: pairResult,
            };
          } else return el;
        })
      );
    },
    []
  );

  const handleEditTimeFrame = useCallback(
    (value: TimeFrame[], pairName: string, exchange: Exchange) => {
      setSelectedExchangeParams((param) =>
        param.map((paramEl) => ({
          ...paramEl,
          sources: paramEl.sources.map((el) => {
            if (paramEl.exchange === exchange && el.pair === pairName) {
              return {
                pair: pairName,
                timeFrame: value,
              };
            } else return el;
          }),
        }))
      );
    },
    []
  );

  const isNextDate = useCallback((date: Dayjs) => {
    return date.isAfter(dayjs().endOf('minute'));
  }, []);

  const isBetweenDate = useCallback(
    (date?: Dayjs) => {
      return !date?.isBetween(startDate, dayjs().endOf('minute'));
    },
    [startDate]
  );

  const handleStart = useCallback(
    (values: IForm) => {
      handleStartStrategy({
        dateStart: values.startDate,
        dateEnd: values.endDate,
        sources: selectedExchangeParams,
        parameters,
      });
    },
    [selectedExchangeParams]
  );

  const handleSubmitForm = useCallback(() => {
    form.submit();
  }, [form]);

  const [pairsData, setPairsData] = useState<Nullable<{[key in Exchange]?: Pair[]; }>>(null);

  useExchangeDataApi(c => {
    c.getPairs()
      .then(d => setPairsData(d))
      .catch(e => { throw e});

  }, [])



  const timeFrames = useMemo(() => {
    return TimeFrameList();
  }, []);


  const [parameters, setParameters] = useState<ParameterModel[]>(params);

  const updateParameters = (newParameters: ParameterModel[]) => {
    setParameters(newParameters);
  };


  return (
    <Modal
      title="Start test"
      open={isModalOpen}
      onOk={handleSubmitForm}
      onCancel={handleCloseModal}
      okText="Run"
      destroyOnClose
      maskClosable={false}
    >
      <Form layout={'vertical'} onFinish={handleStart} form={form}>
          <Form.Item
            name={'startDate'}
            rules={[
              {
                required: true,
                message: "Please select start date",
              },
            ]}
            label="Start date"
          >
            <DatePicker
              style={{
                width: 200,
              }}
              value={dayjs(startDate)}
              disabledDate={isNextDate}
              showTime
              format={'DD.MM.YYYY HH:mm'}
              onChange={(date) => {
                if (date !== null) {
                  setStartDate(date);
                } else {
                  setStartDate(undefined);
                }
              }}
              placeholder="Start date"
            />
          </Form.Item>
          <Form.Item
            dependencies={['startDate']}
            name={'endDate'}
            rules={[
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value?.isBefore(getFieldValue('startDate'))) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    new Error('End date should be after start date')
                  );
                },
              }),
              {
                required: true,
                message: "Please select end date",
              },
            ]}
            label="End date"
          >
            <DatePicker
              style={{width: 200}}
              onChange={(date) => {
                if (date !== null) {
                  setEndDate(date);
                } else {
                  setEndDate(undefined);
                }
              }}
              showTime
              format={'DD.MM.YYYY HH:mm'}
              disabledDate={isBetweenDate}
              value={dayjs(endDate)}
              placeholder="End date"
            />
          </Form.Item>
          {pairsData && (
            <>
              <Form.Item
                name={'exchange'}
                label="Exchange"
                rules={[
                  {
                    required: true,
                    message: "Please select exchange",
                  },
                ]}
              >
                <Select
                  placeholder="Exchange"
                  onChange={handleEditSelectedExchangeParams}
                  value={selectedExchangeParams.map((el) => el.exchange)}
                  mode={'multiple'}
                >
                  {Object.entries(pairsData).map(([key,value]) => (
                    <Option key={key} value={key}>
                      {key}
                    </Option>
                  ))}
                </Select>
              </Form.Item>
              {selectedExchangeParams.length > 0 ? (
                <>
                  {selectedExchangeParams.map((exchange) => (
                    <React.Fragment key={exchange.exchange}>
                      <Divider />
                      <Form.Item
                        name={`pair-${exchange.exchange}`}
                        label={`Pairs for ${exchange.exchange}`}
                        rules={[
                          {
                            required: true,
                            message: "Please select pairs",
                          },
                        ]}
                      >
                        <Select
                          placeholder={"Pairs"}
                          mode={'multiple'}
                          onChange={(values) => {
                            handleEditPairs(values, exchange.exchange);
                          }}
                        >
                          {pairsData[exchange?.exchange]?.map((pair) => (
                              <Option key={pair.first+pair.second} value={`${pair.first}/${pair.second}`}>
                                {`${pair.first}/${pair.second}`}
                              </Option>
                            ))}
                        </Select>
                      </Form.Item>
                      {exchange.sources.map((pair) => (
                        <Form.Item
                          name={`timeframe-${exchange.exchange}-${pair.pair}`}
                          label={`Timeframe for ${pair.pair}`}
                          key={pair.pair}
                          rules={[
                            {
                              required: true,
                              message: "Please select timeframe",
                            },
                          ]}
                        >
                          <Select
                            placeholder="Timeframe"
                            mode={'multiple'}
                            onChange={(value) => {
                              handleEditTimeFrame(
                                value,
                                pair.pair,
                                exchange.exchange
                              );
                            }}
                          >
                            {timeFrames.map((frame) => (
                              <Option key={frame.name} value={frame.id}>
                                {frame.name}
                              </Option>
                            ))}
                          </Select>
                        </Form.Item>
                      ))}
                    </React.Fragment>
                  ))}
                </>
              ) : null}
            </>
          )}
        </Form>
      {parameters.length > 0 &&(
        <ParameterForm parameters={params} onParametersChange={updateParameters}/>
      )}
    </Modal>
  );
};
