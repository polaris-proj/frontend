import {Form, Input, Select} from 'antd';
import React from 'react';
import {ParameterModel} from '~apiClients/DataTypes/ParameterModel';
import {ParameterType} from "~apiClients/DataTypes/ParameterType";

interface Props {
  parameters: ParameterModel[];
  onParametersChange: (newParameters: ParameterModel[]) => void;
}

export const ParameterForm: React.FC<Props> = ({ parameters, onParametersChange }) => {
  const [form] = Form.useForm();

  const handleFormChange = () => {
    const newParameters = form.getFieldsValue();
    onParametersChange(newParameters);
  };

  return (
    <Form form={form} onValuesChange={handleFormChange}>
      {parameters.map((parameter, index) => (
        <Form.Item
          key={index}
          name={parameter.name}
          label={parameter.name}
          initialValue={parameter.value}
        >
          {parameter.typeOfValue === ParameterType.String && <Input />}
          {parameter.typeOfValue === ParameterType.Number && <Input type="number" />}
          {parameter.typeOfValue === ParameterType.Bool && (
            <Select>
              <Select.Option value="true">True</Select.Option>
              <Select.Option value="false">False</Select.Option>
            </Select>
          )}
        </Form.Item>
      ))}
    </Form>
  );
};
