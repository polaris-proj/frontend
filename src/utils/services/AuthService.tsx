import React, { FC, PropsWithChildren, useEffect } from 'react';
import { useAuthSelector } from '../../ducks/auth/authSelectors';
import { useLocation, useNavigate } from 'react-router-dom';
import { Paths } from '../routes/paths';
import { useUserDataSelector } from '../../ducks/user/user-data/userDataSlice';
import { useAppDispatch } from '../../ducks';
import { getUserDataAction } from '../../ducks/user/actions';

export const AuthService: FC<PropsWithChildren> = ({ children }) => {
  const dispatch = useAppDispatch();
  const { authed } = useAuthSelector();
  const { loaded, errorOnLoad } = useUserDataSelector();
  const navigate = useNavigate();
  const location = useLocation();

  useEffect(() => {
    if (authed && !loaded && !errorOnLoad) {
      dispatch(getUserDataAction());
    }
  }, [authed]);

  useEffect(() => {
    if (
      !authed &&
      location.pathname !== Paths.LOGIN &&
      location.pathname !== Paths.REGISTRATION &&
      location.pathname !== Paths.FORGOT_PASSWORD &&
      location.pathname !== Paths.BASE
    ) {
      navigate(Paths.LOGIN);
    }
  }, [authed, location]);

  return <>{children}</>;
};
