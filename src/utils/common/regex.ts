export const Regex = {
  EMAIL: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
  PASSWORD: {
    poor: /[a-z]/,
    weak: /(?=.*?[0-9])/,
    strong: /(?=.*?[#?!@$%^&*-])/,
    whitespace: /^$|\s+/,
  },
};
