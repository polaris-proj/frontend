export const uniqueArray = <T,>(a: T[]): T[] => [...new Set(a)]
export type Dictionary<T> = Record<string, T>
