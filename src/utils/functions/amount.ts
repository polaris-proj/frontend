import { ICurrency, IPair } from '../../types/common/currency';

export const currencyToText: (currency: ICurrency) => string = (currency) => {
  return `${(currency.amount).toLocaleString()} ${currency.currencyType.code}`;
};

export const pairToString = (pair: IPair) => {
  return `${pair.first.code}/${pair.second.code}`;
}
