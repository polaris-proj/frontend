import { IStats } from "../../types/strategies/test";
import { IOrder, OrderStatus, OrderType } from "../../types/common/order";
import { ICurrency } from "../../types/common/currency";



export function calcStats(initialBalance: ICurrency, orders: IOrder[]): IStats {
    let absEarning = orders
        .filter(order => order.status = OrderStatus.Close)
        .map(order => (order.type == OrderType.Sell ? 1 : -1) * order.amount * order.price.amount)
        .reduce((prev, x) => prev + x, 0);
    // todo sell at the end
    let relEarning = absEarning / initialBalance.amount;
    return {
        absEarning: {
            amount: absEarning,
            currencyType: initialBalance.currencyType
        },
        relEarning,
    }
}
