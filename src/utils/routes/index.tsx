import React, { FC } from 'react';
import { Routes, Route } from 'react-router-dom';
import { MainLayout } from '~components/containers/main-layout';
import { Paths } from './paths';
import { Login } from '~pages/login';
import { Registration } from '~pages/registration';
import { Profile } from '~pages/profile';
import { ForgotPassword } from '~pages/forgot-password';
import { WithBreadCrumbs } from '~components/containers/with-bread-crumbs';
import { Strategy } from '~pages/strategy';
import { StrategiesPage } from '~pages/strategies';
import { StrategyTest } from '~pages/run';
import { AccountPage } from "~pages/account/account";

export const RootRoutes: FC = () => {
  return (
    <Routes>
      <Route path={Paths.LOGIN} element={<Login />} />
      <Route path={Paths.REGISTRATION} element={<Registration />} />
      <Route path={Paths.FORGOT_PASSWORD} element={<ForgotPassword />} />
      <Route
        path={'*'}
        element={
          <MainLayout>
            <Routes>
              <Route path={Paths.BASE} element={<Profile />} />
              <Route path={Paths.ACCOUNT} element={<AccountPage />}/>
              <Route
                path={`${Paths.STRATEGYES}/:strategyId`}
                element={
                  <WithBreadCrumbs fullHeight>
                    <Strategy />
                  </WithBreadCrumbs>
                }
              />
              <Route
                path={Paths.STRATEGYES}
                element={
                  <WithBreadCrumbs fullHeight>
                    <StrategiesPage />
                  </WithBreadCrumbs>
                }
              />
              <Route
                path={`${Paths.RUNS}/:runId`}
                element={<StrategyTest />}
              />
            </Routes>
          </MainLayout>
        }
      />
    </Routes>
  );
};
