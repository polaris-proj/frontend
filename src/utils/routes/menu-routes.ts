import { HomeOutlined, BarChartOutlined, ExperimentOutlined } from '@ant-design/icons';
import { Paths } from './paths';
import { TFunction } from 'react-i18next';

interface ISubMenu {
  name: string;
  to: Paths;
}

export interface IMenuRoute {
  name: string;
  to: {
    path: Paths;
    strongEq: boolean;
  }[];
  Icon: typeof HomeOutlined;
  subMenu: false | ISubMenu[];
  notice: false | number;
}

export const MainMenuRoutes: (t: TFunction<string[]>) => IMenuRoute[] = (t) => [
  {
    name: t('menu.home'),
    to: [
      {
        path: Paths.BASE,
        strongEq: true,
      },
    ],
    Icon: HomeOutlined,
    subMenu: false,
    notice: false,
  },
  {
    name: t('menu.strategy'),
    to: [
      {
        path: Paths.STRATEGYES,
        strongEq: false,
      },
    ],
    Icon: BarChartOutlined,
    subMenu: false,
    notice: false,
  }
];
