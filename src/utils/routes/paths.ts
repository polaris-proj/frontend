export enum Paths {
  BASE = '/',
  LOGIN = '/login',
  REGISTRATION = '/registration',
  RUNS = '/run',
  FORGOT_PASSWORD = '/forgot-password',
  STRATEGYES = '/strategy',
  ACCOUNT = '/account'
}
