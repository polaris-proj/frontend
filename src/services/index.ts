import axios from 'axios';
import { accessTokenPath } from '~utils/common/localStoragePaths';
import { HttpError } from '~apiClients/ApiBase/HttpError';

import {authApi} from "~apiClients/hooks/useApi";

export const BASE_API_URL = "/api";

export interface IBaseError<T = any> {
  status: number;
  data: T;
}

export const axiosInstance = axios.create({
  baseURL: BASE_API_URL,
});

axiosInstance.interceptors.request.use((config) => {
  if (config.headers) {
    config.headers.Authorization = `Bearer ${localStorage.getItem(accessTokenPath)}`;
  }
  return config;
});

let isRefreshing = false;

axiosInstance.interceptors.response.use(
  response => {
    // Если запрос был успешным, просто верните ответ
    return response;
  },
  async error => {
    // Если запрос завершился ошибкой, вы можете обработать ее здесь
    if (error.response) {
      // Сервер вернул ответ с кодом ошибки
      if (error.response.status === 401) {
        // Если код ошибки 401, попробуйте обновить токен
        if (isRefreshing) {
          return ;
        }
        isRefreshing = true;
        const refreshToken = localStorage.getItem('refreshToken');
        if (refreshToken) {
          try {
            const newToken = await authApi.refreshToken(refreshToken);
            localStorage.setItem('accessToken', newToken.accessToken);
            localStorage.setItem('refreshToken', newToken.refreshToken);
          }catch (e) {
            isRefreshing = false;
            throw new HttpError(401, 'Refresh token is invalid');
          }
        }
      }
      throw new HttpError(error.response.status, error.response.data);
    } else if (error.request) {
      // Запрос был сделан, но ответа не было
      throw new HttpError(500, 'No response from server');
    } else {
      // Произошла ошибка при настройке запроса
      throw new HttpError(500, error.message);
    }
  }
);
