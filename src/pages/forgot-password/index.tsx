import React, { FC, useCallback } from 'react';
import { AuthWrapper } from '../../components/auth';
import { Form } from 'antd';
import { Input } from '../../components/common/inputs';
import { useTranslation } from 'react-i18next';
import { LocaleKeys } from '../../locale';
import { Helmet } from 'react-helmet-async';

interface IForgotForm {
  email: string;
  newPassword: string;
  newPasswordRepeat: string;
}

export const ForgotPassword: FC = () => {
  const [form] = Form.useForm();
  const { t } = useTranslation([LocaleKeys.FORGOT_PASSWORD]);

  const handleFinish = useCallback(() => {}, []);

  return (
    <>
      <Helmet>
        <title>Forgot password</title>
      </Helmet>
      <AuthWrapper>
        <Form<IForgotForm>
          form={form}
          onFinish={handleFinish}
          style={{
            display: 'flex',
            flexDirection: 'column',
            marginBottom: 30,
          }}
        >
          <Form.Item
            rules={[
              {
                required: true,
                message: t('validation.loginRequired'),
              },
            ]}
            name={'login'}
          >
            <Input
              size={'large'}
              placeholder={t('placeholders.login')}
              autoComplete="off"
            />
          </Form.Item>
          <Form.Item
            rules={[
              {
                required: true,
                message: t('validation.passwordRequired'),
              },
            ]}
            name={'password'}
          >
            <Input
              type={'password'}
              size={'large'}
              placeholder={t('placeholders.password')}
              autoComplete="off"
            />
          </Form.Item>
          <Form.Item
            rules={[
              {
                required: true,
                message: t('validation.passwordRequired'),
              },
            ]}
            name={'password'}
          >
            <Input
              type={'password'}
              size={'large'}
              placeholder={t('placeholders.password-repeat')}
              autoComplete="off"
            />
          </Form.Item>
        </Form>
        <span>s</span>
      </AuthWrapper>
    </>
  );
};
