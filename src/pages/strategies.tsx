import React, { FC, useEffect } from 'react';
import styled from '@emotion/styled';
import { useStrategySelector } from '~ducks/strategies/slices/strategyListSlice';
import { useAppDispatch } from '~/ducks';
import { fetchStrategiesList } from '~ducks/strategies/actions/strategiesList';
import { LoadingOutlined } from '@ant-design/icons';
import { Empty } from 'antd';
import { useTranslation } from 'react-i18next';
import { LocaleKeys } from '~/locale';
import { Helmet } from 'react-helmet-async';
import {StrategyItem} from "~components/strategies-list/strategy-item";

const ListWrapper = styled('div')`
  display: flex;
  flex-wrap: wrap;
  border-radius: 2px;
  background-color: ${({ theme }) => theme.COLORS.WHITE.C100};
  padding-top: 12px;
  padding-bottom: 12px;
  margin-top: 20px;
`;

export const LoadingWrapper = styled('div')`
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const LoadingIcon = styled(LoadingOutlined)`
  color: ${({ theme }) => theme.COLORS.TEXT.COMMON_TEXT};
  font-size: 40px;
`;

export const StrategiesPage: FC = () => {
  const { t } = useTranslation([LocaleKeys.STRATEGY]);
  const dispatch = useAppDispatch();
  const { loaded, loading, errorOnLoad, data } = useStrategySelector();

  useEffect(() => {
    if (!loading && !loaded && !errorOnLoad) {
      dispatch(fetchStrategiesList());
    }
  }, [loaded, loading, errorOnLoad]);

  return (
    <div>
      <Helmet>
        <title>Strategies</title>
      </Helmet>
      <ListWrapper>
        {loading ? (
          <LoadingWrapper>
            <LoadingIcon />
          </LoadingWrapper>
        ) : (
          <>
            {data.length > 0 ? (
                data.map((el) => (<StrategyItem key={el.id} data={el} />))
            ) : (
              <LoadingWrapper>
                <Empty description={t('listPage.date.emptyList')} image={Empty.PRESENTED_IMAGE_SIMPLE}/>
              </LoadingWrapper>
            )}
          </>
        )}
      </ListWrapper>
    </div>
  );
};
