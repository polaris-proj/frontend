import React, { FC, useCallback, useEffect, useState } from 'react';
import { Button, Form, notification } from 'antd';
import { AuthWrapper } from '~components/auth';
import { Paths } from '~utils/routes/paths';
import { AdditionalWrapper } from '../login';
import { AdditionalLink } from '~components/common/links';
import { Regex } from '~utils/common/regex';
import { useTranslation } from 'react-i18next';
import { LocaleKeys } from '../../locale';
import { Input } from '../../components/common/inputs';
import { IBaseError } from '../../services';
import { useNavigate } from 'react-router-dom';
import { Helmet } from 'react-helmet-async';
import {authApi, useAuthApi} from "~apiClients/hooks/useApi";
import {AuthApi} from "~apiClients/Api/AuthApi";
import {RegisterData} from "~apiClients/DataTypes/RegisterData";

interface IRegisterForm {
  email: string;
  password: string;
  passwordRepeat: string;
  login: string;
}

export const Registration: FC = () => {
  const { t, i18n } = useTranslation([LocaleKeys.REGISTRATION]);

  const navigate = useNavigate();

  const [passwordInput, setPasswordInput] = useState('');
  const [passwordRepeatInput, setPasswordRepeatInput] = useState('');

  const [form] = Form.useForm<IRegisterForm>();

  const handleGoToLogin = useCallback(() => {
    navigate(Paths.LOGIN);
    notification.destroy();
  }, []);

  const handleFormSubmit = useCallback(
    (formResult: IRegisterForm) => {
      const dataToSend: RegisterData = {
        login: formResult.login,
        email: formResult.email,
        password: formResult.password,
      };

      authApi.register(dataToSend)
        .then(() => {
          handleGoToLogin();
        })
        .catch((e: IBaseError<null>) => {
          switch (e.status) {
            case 403:
              notification.error({
                description: t('response.errorDescription403'),
                message: t('response.error403'),
                btn: (
                  <Button type={'primary'} onClick={handleGoToLogin}>
                    {t('buttons.alreadyHaveAccount')}
                  </Button>
                ),
                key: String(new Date().getMilliseconds()),
              });
              break;
            case 412:
              notification.error({
                description: t('response.errorDescription412'),
                message: t('response.error412'),
                key: String(new Date().getMilliseconds()),
              });
              break;
          }
        });
    },
    [t]
  );

  return (
    <>
      <Helmet>
        <title>Registration</title>
      </Helmet>
      <AuthWrapper>
        <Form<IRegisterForm>
          form={form}
          onFinish={handleFormSubmit}
          style={{
            display: 'flex',
            flexDirection: 'column',
            marginBottom: 30,
          }}
        >
          <Form.Item
            rules={[
              {
                required: true,
                message: t('validation.emailRequired'),
              },
              {
                pattern: Regex.EMAIL,
                message: t('validation.emailInvalid'),
              },
              {
                max: 63,
                message: t('validation.emailLength'),
              },
            ]}
            name={'email'}
          >
            <Input size={'large'} placeholder={t('placeholders.email')} />
          </Form.Item>
          <Form.Item
            rules={[
              {
                required: true,
                message: t('validation.loginRequired'),
              },
              {
                min: 4,
                message: t('validation.loginMinLength'),
              },
              {
                max: 31,
                message: t('validation.loginMaxLength'),
              },
            ]}
            name={'login'}
          >
            <Input size={'large'} placeholder={t('placeholders.login')} />
          </Form.Item>
          <Form.Item
            rules={[
              {
                required: true,
                message: t('validation.passwordRequired'),
              },
              {
                min: 13,
                message: t('validation.passwordMinLength'),
              },
              {
                max: 63,
                message: t('validation.passwordMaxLength'),
              },
            ]}
            name={'password'}
          >
            <Input
              type={'password'}
              size={'large'}
              placeholder={t('placeholders.password')}
              value={passwordInput}
              onChange={(e) => {
                setPasswordInput(e.target.value);
              }}
            />
          </Form.Item>
          <Form.Item
            rules={[
              {
                required: true,
                message: t('validation.passwordRepeatRequired'),
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (value === getFieldValue('password')) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    new Error(t('validation.passwordsInComp'))
                  );
                },
              }),
            ]}
            dependencies={['password']}
            name={'passwordRepeat'}
          >
            <Input
              type={'password'}
              size={'large'}
              placeholder={t('placeholders.passwordRepeat')}
              value={passwordRepeatInput}
              onChange={(e) => {
                setPasswordRepeatInput(e.target.value);
              }}
            />
          </Form.Item>
          <Button
            type={'primary'}
            size={'large'}
            onClick={form.submit}
          >
            {t('buttons.registration')}
          </Button>
        </Form>
        <AdditionalWrapper>
          <AdditionalLink to={Paths.LOGIN}>
            {t('buttons.alreadyHaveAccount')}
          </AdditionalLink>
        </AdditionalWrapper>
      </AuthWrapper>
    </>
  );
};
