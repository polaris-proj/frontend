import React, { FC } from 'react';
import styled from '@emotion/styled';
import main from '../../images/main.png';
import Title from 'antd/es/typography/Title';
import { Button, Typography } from 'antd';
import { Paths } from '../../utils/routes/paths';
import { useNavigate } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { LocaleKeys } from '~locale/index';


const Background = styled('div')`
  background-image: url(${main});
  height: 600px;
  display: flex;
  align-items: center;
`;

const MainContent = styled('div')`
  display: flex;
  flex-direction: column;
  align-items: center;
  max-width: 600px;
  margin: 30px;

`
export const Profile: FC = () => {
  const { t } = useTranslation([LocaleKeys.MAIN]);
  const navigate = useNavigate();
  return (
        <Background>
          <MainContent>
            <Title>POLARIS PROJECT</Title>
            <Typography.Paragraph>
              {t('welcome')}
            </Typography.Paragraph>
            <Button
              type={'primary'}
              onClick={() => navigate(Paths.STRATEGYES)}
            >
              {t('button')}
            </Button>
          </MainContent>
        </Background>
  );
};
