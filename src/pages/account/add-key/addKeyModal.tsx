import React, { useEffect, useState } from "react";
import './addKeyModal.css';
import { useUserApi, userApi } from '~apiClients/hooks/useApi';
import { ExchangeKey } from '~apiClients/DataTypes/ExchangeKey';

export default function Modal({ modal, setModal }) {
  const toggleModal = () => {
    setModal(!modal);
  };
  if (modal) {
    document.body.classList.add('active-modal');
  } else {
    document.body.classList.remove('active-modal');
  }

  const handleSubmit = async (e: any) => {
    e.preventDefault();
    const exchange = e.target.elements.exchange.value;
    const token = e.target.elements.token.value;
    const secret = e.target.elements.secret.value;
    const exchangeKey: ExchangeKey = { exchange, token, secret };
    try {
      await userApi.addExchangeKey(exchangeKey);
      toggleModal();
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <>
      {modal && (
        <div className="modal">
          <div onClick={toggleModal} className="overlay"/>
          <div className="modal-content">
            <form onSubmit={handleSubmit}>
              <div className="form-group">
                <label>
                  EXCHANGE
                  <select name="exchange">
                    <option value="0">Binance Spot</option>
                    <option value="1">Poloniex</option>
                    <option value="3">Binance Futures</option>
                  </select>
                </label>
              </div>
              <div className="form-group">
                <label>
                  KEY
                  <input name="token" required/>
                </label>
              </div>
              <div className="form-group">
                <label>
                  SECRET
                  <input name="secret" type="password" required/>
                </label>
              </div>
              <button className="add-key-btn">
                Add API-key
              </button>
              <button className="close-modal" onClick={toggleModal}>
                Close
              </button>
            </form>
          </div>
        </div>
      )}
    </>
  );
}
