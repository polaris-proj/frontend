import React, { useMemo } from "react";
import { useTable, useSortBy } from "react-table";
import "./table.css";
import {ExchangeKeyDto} from "~apiClients/DataTypes/ExchangeKeyDto";
import {userApi} from "~apiClients/hooks/useApi";

export const Table = ({ data } : {data: ExchangeKeyDto[]}) => {
  const columns = useMemo(
    () => [
      { Header: "Exchange", accessor: "exchange" },
      { Header: "Key", accessor: "key" },
      {
        Header: "",
        accessor: "delete",
        Cell: ({ row }: { row: { original: ExchangeKeyDto } }) => (
          <button onClick={() => handleDelete(row.original)}>
            X
          </button>
        ),
      },
    ],
    []
  );
  const handleDelete = async (exchangeKey: ExchangeKeyDto) => {
    try {
      await userApi.removeExchangeKey(exchangeKey.exchange);

    } catch (error) {
      console.error(error);
    }
  };
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow
  } = useTable({ columns, data }, useSortBy);

  return (
    <div className="table-container">
      <table {...getTableProps()}>
        <thead>
        {headerGroups.map((headerGroup) => (
          <tr {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map((column) => (
              <th {...column.getHeaderProps()}>{column.render("Header")}</th>
            ))}
          </tr>
        ))}
        </thead>
        <tbody {...getTableBodyProps()}>
        {rows.map((row) => {
          prepareRow(row);
          return (
            <tr {...row.getRowProps()}>
              {row.cells.map((cell) => (
                <td {...cell.getCellProps()}>{cell.render("Cell")}</td>
              ))}
            </tr>
          );
        })}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
