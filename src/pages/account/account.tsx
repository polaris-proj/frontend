import React, { useEffect, useRef, useState } from 'react';
import './account.css';
import { PlusOutlined } from '@ant-design/icons';
import { useAppDispatch } from '~/ducks';
import AddKeyModal from '~pages/account/add-key/addKeyModal';
import Table from "~pages/account/create-table/table";
import { Helmet } from "react-helmet-async";
import {useUserDataSelector} from "~ducks/user/user-data/userDataSlice";
import {User} from "~apiClients/DataTypes/User";
import {getUserDataAction} from "~ducks/user/actions";
import {userApi, useUserApi} from "~apiClients/hooks/useApi";
import {ExchangeKeyDto} from "~apiClients/DataTypes/ExchangeKeyDto";

export const AccountPage = () => {
  const dispatch = useAppDispatch();

  const [modal, setModal] = useState(false);
  const t = useUserDataSelector();
  const [user, setUser] = useState<Nullable<User>>(null);

  useEffect(() => {
    dispatch(getUserDataAction);
    setUser(t.data);
  }, [dispatch, t]);
  const toggleModal = () => {
    setModal(!modal);
  };

  const [data, setData] = useState<ExchangeKeyDto[]>([]);
// Функция для обновления данных
  const refreshData = () => {
      userApi.getExchangeKeys()
        .then((agents) => {
          setData(agents);
        })
        .catch((error) => {
          console.log(error);
        });
  };

  useEffect(() => {
    refreshData();
  }, []);


  const handleCloseModal = () => {
    setModal(false);
    refreshData();
    console.log('close');
  };
  return (
    <div className="account-page">
      <Helmet>
        <title>Account</title>
      </Helmet>
      <div className="name-card">
        <div className="container">
          <div className="image">
            <img
              src={user?.photo??"src/mocks/images/main-user.jpg"}
              className="rounded"
              alt="Profile Photo"
            />
          </div>
          <div className="middle">
            <img src="src/mocks/images/overlay-plus.png" className="rounded"/>
          </div>
        </div>

        <div className="name-card__content">
          <div className="form-group">
            <label>NAME<input name="Name" placeholder="Your name" /></label>
          </div>
          <div className="form-group">
            <label>DESCRIPTION<input name="Description" placeholder="Add description..." /></label>
          </div>
          <div className="form-group">
            <label>STATUS<input name="Status" placeholder="Write something for your status..."/></label>
          </div>
        </div>
      </div>

      <div className="api-keys-block">
        <h1>API keys</h1>
        <button type="button" onClick={toggleModal} className="btn-modal">
          <div className="plus">
            <PlusOutlined />
          </div>
          Add new key
        </button>
        {data&&<Table data={data}/>}

      </div>

      <AddKeyModal modal={modal} setModal={handleCloseModal} />
    </div>
  );
}
