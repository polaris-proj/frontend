import React, {FC, useState} from 'react';
import { StrategyHeader } from '~components/strategy/strategy-header';
import { StrategyMainContent } from '~components/strategy/strategy-main-content';
import { useParams } from 'react-router-dom';
import { LoadingIcon, LoadingWrapper } from '../strategies';
import { Helmet } from "react-helmet-async";
import {useStrategyApi} from "~apiClients/hooks/useApi";
import {StrategyWithParameters} from "~apiClients/DataTypes/StrategyWithParameters";
import {TestResult} from "~apiClients/DataTypes/TestResult";

export const Strategy: FC = () => {
  const [strategyParameters, setStrategyParameters] = useState<Nullable<StrategyWithParameters>>(null);
  const [testRuns, setTestRuns] = useState<Nullable<TestResult[]>>(null);
  const params = useParams<{ strategyId: string; }>();

  if(params?.strategyId)
  {
    useStrategyApi(c => {
      c.getStrategy(params.strategyId)
        .then(d => setStrategyParameters(d))
        .catch(e => {
          throw e
        });
      c.getStrategyTestRuns(params.strategyId)
        .then(d=> setTestRuns(d))
        .catch(e => {
          throw e
        });
    }, [params])
  }

  const loaded = strategyParameters != null && testRuns != null;

  return (
    <div>
      <Helmet>
        <title>Polaris</title>
      </Helmet>
      {!loaded ? (
        <LoadingWrapper>
          <LoadingIcon />
        </LoadingWrapper>
      ) : (
        <>
          <StrategyHeader strategy={strategyParameters} />
          <StrategyMainContent strategy={strategyParameters} />
        </>
      )}
    </div>
  );
};
