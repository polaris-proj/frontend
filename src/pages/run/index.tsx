import React, {FC, useState} from 'react';
import { useParams } from 'react-router-dom';
import { LoadingWrapper } from '../strategies';
import { Empty } from 'antd';
import {ReportViewer} from "~charts/ReportViewer";
import {strategyApi, useStrategyApi} from "~apiClients/hooks/useApi";

export const StrategyTest: FC = () => {
  const [startTime, setStartTime] = useState<Date|string|null>();
  const [endTime, setEndTime] = useState<Date|string|null>();
  const params = useParams<{
    runId: string;
  }>();

  useStrategyApi(c => {
      strategyApi.getTestStrategyParameters(params.runId)
        .then((cc)=>{
          setStartTime(cc.startTime);
          setEndTime(cc.endTime);
        })
        .catch(e => {throw e;});
    }, [params.runId]
  );

  console.log(params.runId, startTime, endTime)

  return (
    <div>
          {params.runId && startTime && endTime ? (
            <ReportViewer runId={params.runId} startTime={startTime} endTime={endTime}/>
          ) : (
            <LoadingWrapper>
              <Empty
                description={"Startegies not found"}
                image={Empty.PRESENTED_IMAGE_SIMPLE}
              />
            </LoadingWrapper>
          )}
    </div>
  );
};
