import React, { FC, useCallback, useEffect, useState } from 'react';
import { AuthWrapper } from '../../components/auth';
import styled from '@emotion/styled';
import { Form, Button, notification } from 'antd';
import { Paths } from '../../utils/routes/paths';
import { AdditionalLink } from '../../components/common/links';
import { useTranslation } from 'react-i18next';
import { LocaleKeys } from '../../locale';
import { Input } from '../../components/common/inputs';
import { useAppDispatch } from '../../ducks';
import { clearError, loginAction } from '../../ducks/auth/authSlice';
import { useAuthSelector } from '../../ducks/auth/authSelectors';
import { useNavigate } from 'react-router-dom';
import { Helmet } from 'react-helmet-async';

export const AdditionalWrapper = styled('div')`
  display: flex;
  justify-content: space-between;
  margin-top: 68px;
  padding-top: 12px;
  border-top: 1px solid ${({ theme }) => theme.COLORS.WHITE.C300};
  transition: border-top-color 0.3s ease-out;
`;

interface FormData {
  login: string;
  password: string;
}

export const Login: FC = () => {
  const { t } = useTranslation([LocaleKeys.LOGIN]);
  const [isLoading, setIsLoading] = useState(false);
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const { error, authed } = useAuthSelector();

  const [form] = Form.useForm<FormData>();

  useEffect(() => {
    document.title = 'Login';
  }, []);

  useEffect(() => {
    if (authed) {
      navigate(Paths.BASE);
    }
  }, [authed]);

  useEffect(() => {
    if (error.isError) {
      switch (error.status) {
        case 403:
          notification.error({
            message: t('response.message403'),
            description: t('response.description403'),
          });
          break;
        case 401:
            notification.error({
                message: t('response.message401'),
                description: t('response.description401'),
            });
            break;
      }

      dispatch(clearError());
    }
  }, [error.isError]);

  const handleFinish = useCallback((formData: FormData) => {
    setIsLoading(true);
    dispatch(
      loginAction({
        ...formData,
        loadingCallBack: () => {
          setIsLoading(false);
        },
      }));
  }, []);

  return (
    <>
      <Helmet>
        <title>Authorization</title>
      </Helmet>

      <AuthWrapper>
        <Form<FormData>
          form={form}
          onFinish={handleFinish}
          style={{
            display: 'flex',
            flexDirection: 'column',
            marginBottom: 30,
          }}
        >
          <Form.Item
            rules={[
              {
                required: true,
                message: t('validation.loginRequired'),
              },
            ]}
            name={'login'}
          >
            <Input
              size={'large'}
              placeholder={t('placeholders.login')}
              autoComplete="off"
            />
          </Form.Item>
          <Form.Item
            rules={[
              {
                required: true,
                message: t('validation.passwordRequired'),
              },
            ]}
            name={'password'}
          >
            <Input
              type={'password'}
              size={'large'}
              placeholder={t('placeholders.password')}
              autoComplete="off"
            />
          </Form.Item>
          <Button
            type={'primary'}
            size={'large'}
            onClick={form.submit}
            loading={isLoading}
          >
            {t('buttons.login')}
          </Button>
        </Form>
        <AdditionalWrapper>
          <AdditionalLink to={Paths.REGISTRATION}>
            {t('buttons.registration')}
          </AdditionalLink>
          {/*<AdditionalLink to={Paths.FORGOT_PASSWORD}>*/}
          {/*  {t('buttons.forgotPassword')}*/}
          {/*</AdditionalLink>*/}
        </AdditionalWrapper>
      </AuthWrapper>
    </>
  );
};
