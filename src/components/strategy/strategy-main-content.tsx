import React, { FC, useState } from 'react';
import styled from '@emotion/styled';
import { BaseContentWrapper } from '../common/blocks';
import { Divider } from 'antd';
import { StrategyDescription } from './components/strategy-description';
import { TestsTable } from '../tests-list/test-of-strategies-table';
import {strategyApi, useStrategyApi} from "~apiClients/hooks/useApi";
import {TestResult} from "~apiClients/DataTypes/TestResult";
import {StrategyWithParameters} from "~apiClients/DataTypes/StrategyWithParameters";

interface Props {
  strategy: StrategyWithParameters;
}

const Wrapper = styled(BaseContentWrapper)`
  margin-top: 20px;
  padding-top: 12px;
  padding-bottom: 30px;
`;

export const StrategyMainContent: FC<Props> = ({ strategy}) => {
  const [tests, setTests] = useState<TestResult[]>([]);

  useStrategyApi(c => {
      strategyApi.getStrategyTestRuns(strategy.id)
        .then(setTests)
        .catch(e => {throw e;});
  }, [strategy.id]
  );

  // const allProfitChart = useMemo(() => {
  //   const result: IStrategy['charts']['recentEarnings'] = [];
  //   let sum = 0;

  //   strategy.charts.recentEarnings.forEach((item) => {
  //     result.push({
  //       time: item.time,
  //       profit: item.profit + sum,
  //     });

  //     sum += item.profit;
  //   });

  //   return result;
  // }, [strategy.charts.recentEarnings]);

  // const profitStatChart = useMemo(() => {
  //   const profitBar: SeriesDataItemTypeMap['Histogram'][] = [];
  //   const unprofitBar: SeriesDataItemTypeMap['Histogram'][] = [];

  //   const firstItem = strategy.charts.recentEarnings[0];

  //   let periodUnprofitCount = 0;
  //   let periodProfitCount = 0;

  //   let periodStartEndOf: unitOfTime.StartOf = 'day';

  //   switch (statProfitPeriod) {
  //     case Period.YEAR:
  //       periodStartEndOf = 'year';
  //       break;
  //     case Period.WEEK:
  //       periodStartEndOf = 'week';
  //       break;
  //     case Period.MONTH:
  //       periodStartEndOf = 'month';
  //       break;
  //     case Period.DAY:
  //       periodStartEndOf = 'day';
  //       break;
  //   }

  //   let lastDate = dayjs.unix(firstItem.time).endOf(periodStartEndOf);

  //   strategy.charts.recentEarnings.forEach((item) => {
  //     if (dayjs.unix(item.time).isBefore(lastDate)) {
  //       if (item.profit > 0) {
  //         periodProfitCount++;
  //       } else {
  //         periodUnprofitCount++;
  //       }
  //     } else {
  //       profitBar.push({
  //         time: lastDate.unix() as UTCTimestamp,
  //         value: periodProfitCount,
  //         color: theme.COLORS.CHARTS.UP_COLOR,
  //       });
  //       unprofitBar.push({
  //         time: lastDate.add(1, 'second').unix() as UTCTimestamp,
  //         value: periodUnprofitCount,
  //         color: theme.COLORS.CHARTS.DOWN_COLOR,
  //       });
  //       unprofitBar.push({
  //         time: lastDate.add(2, 'second').unix() as UTCTimestamp,
  //       });

  //       periodUnprofitCount = item.profit > 0 ? 0 : 1;
  //       periodProfitCount = item.profit > 0 ? 1 : 0;
  //       lastDate = dayjs.unix(item.time).endOf(periodStartEndOf);
  //     }
  //   });

  //   return {
  //     profitBar,
  //     unprofitBar,
  //   };
  // }, [statProfitPeriod]);

  return (
    <Wrapper>
      {/* <Tabs
        items={tabItems}
      /> */}
      <StrategyDescription data={[{title:"Example title", text:"example text"}]} />
      <Divider />
      {/* <RunParams data={strategy.properties} t={t} /> */}
      {/* <BaseLineChart
        height={300}
        period={recentEarningsPeriod}
        setPeriod={setRecentEarningsPeriod}
        title={t('currentPage.charts.recentEarningsTitle')}
        chartData={strategy.charts.recentEarnings}
      />
      <CandlestickWithDealsChart
        height={600}
        title={t('currentPage.charts.allDealsTitle')}
      />
      <BaseLineChart
        height={300}
        title={t('currentPage.charts.allProfitTitle')}
        chartData={allProfitChart}
        isWithPeriod={false}
      />
      <HistogramChart
        title={t('currentPage.charts.countDealsTitle')}
        period={statProfitPeriod}
        onChangePeriod={setStatProfitPeriod}
        height={300}
        data1={profitStatChart.profitBar}
        data2={profitStatChart.unprofitBar}
      /> */}
      <TestsTable
        data={tests}
      />
    </Wrapper>
  );
};
