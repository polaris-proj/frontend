import React, { FC, useCallback, useState } from 'react';
import { Dayjs } from 'dayjs';
import { StartButton } from '../common/buttons';
import { BaseContentWrapper } from '../common/blocks';
import { IExchangedParamsItem } from '~ducks/strategies/slices/exchangeParamsSlice';
import { StartStrategyModal } from '~modals/start-strategy-modal';
import styled from '@emotion/styled';
import {StrategyWithParameters} from "~apiClients/DataTypes/StrategyWithParameters";
import {strategyApi} from "~apiClients/hooks/useApi";
import {ExchangeSources} from "~apiClients/DataTypes/ExchangeSources";
import {ParameterModel} from "~apiClients/DataTypes/ParameterModel";
import {Pair} from "~apiClients/DataTypes/Pair";
import {notification} from "antd";

interface Props {
  strategy: StrategyWithParameters;
}

const HeadWrapper = styled(BaseContentWrapper)`
  gap: 40px;
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

const HeadMainInfoWrapper = styled('div')`
  display: flex;
  flex-direction: column;
  gap: 20px;
  justify-content: center;
`;

const Title = styled('span')`
  font-weight: 700;
  font-size: 26px;
  line-height: 26px;
  overflow-wrap: anywhere;
  color: ${({ theme }) => theme.COLORS.TEXT.COMMON_TEXT};
`;


const HeadAsideMiddle = styled('div')`
  display: flex;
  flex-wrap: wrap;
  gap: 12px;
  padding-left: 10px;
  padding-right: 30px;
`;

export const StrategyHeader: FC<Props> = ({ strategy }) => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const handleCloseModal = () => {
    setIsModalOpen(() => false);
  };

  const handleOpenModal = () => {
    setIsModalOpen(() => true);
  };

  const convertToExchangeSources = (exchangeParams: IExchangedParamsItem[]): ExchangeSources[] => {
    return exchangeParams.map(item => (
      {
        exchange: item.exchange,
        pairTimeFrame: item.sources.flatMap(source => {
        const pair: Pair = {
          first: source.pair.split("/")[0],
          second: source.pair.split("/")[1]
        }
        return source.timeFrame.map(tf => ({pair, timeFrame: tf}));
      })
    }));
  }

  const handleStartStrategy = useCallback(
    async (some: {
      dateStart: Dayjs;
      dateEnd: Dayjs;
      sources: IExchangedParamsItem[];
      parameters: ParameterModel[];
    }) => {
      notification.info({
        message: "Start test",
        description: 'please wait',
      });
      strategyApi.startTestOfStrategy({
        strategyId: strategy.id,
        parameters: some.parameters,
        allowedSource: convertToExchangeSources(some.sources),
        initialBalance: 100000,
        isRealTimeTestMode: false,
        enableOrderConfirmation: false,
        startTime: some.dateStart.toISOString(),
        endTime: some.dateEnd.toISOString(),
      })
        .then((e)=>{
          notification.success({
            message: "Test completed",
            description: "Update page to see info"
          })
          setIsModalOpen(() => false);
        })
        .catch((e)=>{
          console.error(e);
        });
    },
    [strategy.id, handleCloseModal]
  );


  return (
    <HeadWrapper>
      <HeadMainInfoWrapper>
        <Title>{strategy.internalName}</Title>
      </HeadMainInfoWrapper>
      <HeadAsideMiddle>
        <StartButton
          onClick={handleOpenModal}>
          Start test
        </StartButton>
      </HeadAsideMiddle>
      <StartStrategyModal
        handleStartStrategy={handleStartStrategy}
        handleCloseModal={handleCloseModal}
        isModalOpen={isModalOpen}
        params={strategy.properties}
      />
    </HeadWrapper>
  );
};
