import React, { FC, useMemo } from 'react';
import {
  CaretDownOutlined,
  CaretUpOutlined,
  StarOutlined,
} from '@ant-design/icons';
import styled from '@emotion/styled';
import { Theme } from '@emotion/react';

const HeadAsideTopItem = styled('div')`
  display: flex;
  flex-direction: column;
`;

const HeadAsideTopItemText = styled('div')`
  display: flex;
  align-items: center;
  gap: 6px;
`;

const HeadAsideTopItemValue = styled('div')`
  display: flex;
  align-items: center;
  gap: 6px;
  font-weight: 700;
  font-size: 14px;
  line-height: 22px;
  padding-left: 22px;
  color: ${({ theme }) => theme.COLORS.ACCENT.SECONDARY};
`;

const HeadAsideTopItemTextContent = styled('div')`
  font-weight: 400;
  font-size: 14px;
  line-height: 22px;
  color: ${({ theme }) => theme.COLORS.ACCENT.SECONDARY};
`;

const TextedIcon = styled('span')`
  font-weight: 700;
  font-size: 10px;
  line-height: 12px;
`;

interface Props {
  icon: JSX.Element;
  title: string;
  value: string | number;
  marker: {
    type: 'star' | 'inc' | 'dec' | string;
    danger: boolean;
  };
  theme: Theme;
}

export const HeadFeatureItem: FC<Props> = ({
  icon,
  title,
  value,
  marker,
  theme,
}) => {
  const baseStyles = useMemo(() => {
    return {
      color: marker.danger
        ? theme.COLORS.ACCENT.DANGER
        : theme.COLORS.ACCENT.SUCCESS,
    };
  }, [marker, theme]);

  const Marker = useMemo(() => {
    switch (marker.type) {
      case 'star':
        return <StarOutlined style={baseStyles} />;
      case 'dec':
        return <CaretDownOutlined style={baseStyles} />;
      case 'inc':
        return <CaretUpOutlined style={baseStyles} />;
      default:
        return <TextedIcon style={baseStyles}>{marker.type}</TextedIcon>;
    }
  }, [marker, baseStyles]);

  return (
    <HeadAsideTopItem>
      <HeadAsideTopItemText>
        {icon}
        <HeadAsideTopItemTextContent>{title}</HeadAsideTopItemTextContent>
      </HeadAsideTopItemText>
      <HeadAsideTopItemValue>
        {value}
        {Marker}
      </HeadAsideTopItemValue>
    </HeadAsideTopItem>
  );
};
