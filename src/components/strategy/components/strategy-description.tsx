import React, { FC } from 'react';
import { Space, Typography } from 'antd';
import styled from '@emotion/styled';

interface Props {
  data: { title: string; text: string }[];
}

const Wrapper = styled(Space)`
  margin-top: 2px;
  max-width: 600px;
`;

export const StrategyDescription: FC<Props> = ({ data }) => {
  if (!data) {
    // todo
    data = [{title: "oopps", text: "no descrition"}];
  }

  return (
    <Wrapper direction={'vertical'} size={'middle'}>
      {data.map((item, index) => (
        <Space key={index} direction={'vertical'} size={'middle'}>
          <Typography.Title
            style={{
              margin: "20px",
            }}
            level={4}
          >
            {item.title}
          </Typography.Title>
          <Typography.Paragraph
            style={{
              margin: "20px",
              marginTop: "0px"
            }}
          >
            {item.text}
          </Typography.Paragraph>
        </Space>
      ))}
    </Wrapper>
  );
};
