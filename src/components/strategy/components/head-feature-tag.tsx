import React, { FC, PropsWithChildren } from 'react';
import styled from '@emotion/styled';
import { Sizes } from '../../../styles';

const Tag = styled('div')`
  padding: 1px 8px;
  background: ${({ theme }) => theme.COLORS.WHITE.C400};
  border: 1px solid ${({ theme }) => theme.COLORS.WHITE.C900};
  border-radius: ${Sizes.BORDER_RADIUS_SM};
`;

export const HeadFeatureTag: FC<PropsWithChildren> = ({ children }) => {
  return <Tag>{children}</Tag>;
};
