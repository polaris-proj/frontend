import React, { FC, useState } from 'react';
import styled from '@emotion/styled';
import { NavLink } from 'react-router-dom';
import { Paths } from '~utils/routes/paths';
import {
  DollarCircleOutlined,
  StarOutlined,
  TransactionOutlined,
} from '@ant-design/icons';
import { Descriptions, DescriptionsProps, Tooltip } from 'antd';
import { ZIndex } from '../../styles/z-index';
import { StyledPrice } from '../common/price';
import { useAppDispatch } from '../../ducks';
import {StrategyWithParameters} from "~apiClients/DataTypes/StrategyWithParameters";

const Wrapper = styled(NavLink)`
  text-decoration: none;
  flex-basis: calc(33.3% - 16px);
  margin-bottom: 12px;
  margin-left: 12px;
  border: 1px solid ${({ theme }) => theme.COLORS.WHITE.C400};
  background-color: ${({ theme }) => theme.COLORS.WHITE.C200};
  border-radius: 2px;
  padding: 10px;
  height: 130px;
  display: flex;
  position: relative;
  flex-direction: column;
  justify-content: space-between;
`;

const Line = styled('div')`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  z-index: ${ZIndex.Z2};
`;

const BluredBlock = styled('span')`
  display: flex;
  backdrop-filter: blur(2.5px);
  border-radius: 2px;
  background-color: ${({ theme }) => theme.COLORS.WHITE.C100_08};
  margin-bottom: 10px;
`;

const DefaultText = styled('span')`
  font-weight: 400;
  font-size: 14px;
  line-height: 22px;
  letter-spacing: 0.01em;
  color: ${({ theme }) => theme.COLORS.TEXT.HEADING};
`;

const PriceText = styled('span')`
  color: ${({ theme }) => theme.COLORS.TEXT.COMMON_TEXT};
  font-weight: 400;
  font-size: 14px;
  line-height: 22px;
`;

const AccentText = styled('span')<{ danger?: boolean }>`
  color: ${({ theme, danger }) =>
    danger ? theme.COLORS.ACCENT.DANGER : theme.COLORS.ACCENT.SUCCESS};
  font-weight: 700;
  font-size: 20px;
  line-height: 22px;
`;

const StarIcon = styled(StarOutlined)`
  color: ${({ theme }) => theme.COLORS.ACCENT.SUCCESS};
  margin-left: 6px;
`;

const DollarIcon = styled(DollarCircleOutlined)`
  color: ${({ theme }) => theme.COLORS.TEXT.COMMON_TEXT};
  margin-right: 8px;
`;

const TransactionIcon = styled(TransactionOutlined)`
  color: ${({ theme }) => theme.COLORS.TEXT.COMMON_TEXT};
  margin-right: 8px;
`;

const ChartWrapper = styled('div')`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  pointer-events: none;
  user-select: none;
`;

interface Props {
  data: StrategyWithParameters;
}

export const StrategyItem: FC<Props> = ({ data}) => {

  const dispatch = useAppDispatch();

  const [testsCount, setTestsCount] = useState(0);
  const [avgRelEarning, SetAvgRelEarning] = useState(() => NaN);



  // const testsCount: number = tests.length;
  // const finateTests: ITest[] = tests
  //   .filter(test => isFinite(test.relEarning));
  // const avgRelEarning = Math.round(10000 * finateTests
  //   .reduce((prev, test) => prev + test.relEarning, 0) / finateTests.length) / 100;

  // todo translation
  const items: DescriptionsProps['items'] = [
    {
      key: 0,
      label: 'Количество тестов',
      children: testsCount
    },
    {
      key: 1,
      label: 'Стредний профит',
      children: avgRelEarning ? <StyledPrice danger={avgRelEarning < 0}>{avgRelEarning}%</StyledPrice> : 'Нет данных'
    }
  ];

  // const chartContainer = useRef<HTMLDivElement>(null);

  // const [chart, setChart] = useState<IChartApi | null>(null);
  // const [areaSeries, setAreaSeries] = useState<ISeriesApi<'Area'> | null>(null);

  // const dataToSerialize = useMemo(() => {
  //   const count =
  //     data.charts.recentEarnings.length < 50
  //       ? 0
  //       : data.charts.recentEarnings.length - 50;

  //   return data.charts.recentEarnings
  //     .slice(count, data.charts.recentEarnings.length - 1)
  //     .map((el) => {
  //       return {
  //         time: el.time as UTCTimestamp,
  //         value: el.profit,
  //       };
  //     });
  // }, [data.charts.recentEarnings]);

  // const chartOption: DeepPartial<ChartOptions> = useMemo(() => {
  //   return {
  //     layout: {
  //       textColor: theme.COLORS.TEXT.COMMON_TEXT,
  //       background: {
  //         type: ColorType.Solid,
  //         color: theme.COLORS.TRANSPARENT,
  //       },
  //     },
  //     grid: {
  //       vertLines: {
  //         visible: false,
  //       },
  //       horzLines: {
  //         visible: false,
  //       },
  //     },
  //     crosshair: {
  //       mode: CrosshairMode.Normal,
  //       horzLine: {
  //         visible: false,
  //       },
  //       vertLine: {
  //         visible: false,
  //       },
  //     },
  //     timeScale: {
  //       rightOffset: -1,
  //       fixLeftEdge: true,
  //       visible: false,
  //       lockVisibleTimeRangeOnResize: true,
  //     },
  //     rightPriceScale: {
  //       visible: false,
  //     },
  //     handleScroll: {
  //       mouseWheel: false,
  //       pressedMouseMove: false,
  //       horzTouchDrag: false,
  //       vertTouchDrag: false,
  //     },
  //     handleScale: {
  //       axisPressedMouseMove: false,
  //       mouseWheel: false,
  //       pinch: false,
  //     },
  //   };
  // }, [theme]);

  // const chartAreaSeriesOptions: AreaSeriesPartialOptions = useMemo(() => {
  //   return {
  //     crosshairMarkerVisible: false,
  //     priceLineVisible: false,
  //     baseLineVisible: false,
  //     baseLineWidth: 1,
  //     lineWidth: 1,
  //     lineColor: data.feature.averageReturn.isIncreased
  //       ? theme.COLORS.CHARTS.UP_COLOR
  //       : theme.COLORS.CHARTS.DOWN_COLOR,
  //     crosshairMarkerBackgroundColor: theme.COLORS.TRANSPARENT,
  //     topColor: theme.COLORS.TRANSPARENT,
  //     bottomColor: theme.COLORS.TRANSPARENT,
  //   };
  // }, [theme, data]);

  // useEffect(() => {
  //   if (chartContainer.current) {
  //     const newChart = createChart(chartContainer.current, {
  //       ...chartOption,
  //       timeScale: {
  //         ...chartOption.timeScale,
  //         barSpacing:
  //           chartContainer.current.clientWidth / (dataToSerialize.length - 2),
  //       },
  //     });

  //     const newAreaSeries = newChart.addAreaSeries(chartAreaSeriesOptions);

  //     newAreaSeries.setData(dataToSerialize);

  //     setAreaSeries(newAreaSeries);
  //     setChart(newChart);
  //   }

  //   return () => {
  //     if (chartContainer.current) {
  //       chartContainer.current.innerHTML = '';
  //     }
  //   };
  // }, []);

  // useEffect(() => {
  //   if (chart) {
  //     chart.applyOptions({
  //       ...chartOption,
  //       timeScale: {
  //         ...chartOption.timeScale,
  //         barSpacing:
  //           (chartContainer?.current?.clientWidth ?? 200) /
  //           (dataToSerialize.length - 2),
  //       },
  //     });
  //   }
  // }, [chartOption]);

  // useEffect(() => {
  //   if (areaSeries) {
  //     areaSeries.applyOptions(chartAreaSeriesOptions);
  //   }
  // }, [chartAreaSeriesOptions]);

  // useEffect(() => {
  //   const listener = () => {
  //     if (chart) {
  //       chart.applyOptions({
  //         ...chartOption,
  //         timeScale: {
  //           ...chartOption.timeScale,
  //           barSpacing:
  //             (chartContainer?.current?.clientWidth ?? 200) /
  //             (dataToSerialize.length - 2),
  //         },
  //       });
  //     }
  //   };

  //   window.addEventListener('resize', listener);

  //   return () => {
  //     window.removeEventListener('resize', listener);
  //   };
  // }, [chart, chartOption, dataToSerialize]);

  return (
    <Wrapper to={`${Paths.STRATEGYES}/${data.id}`}>
      <Line>
        <BluredBlock
          style={{
            marginRight: 10,
          }}
        >
          <Tooltip title={"Strategy name"}>
            <DefaultText>{data.internalName}</DefaultText>
          </Tooltip>
        </BluredBlock>
        <Descriptions
          column={1}
          items={items}
        />
        {/* <BluredBlock
          style={{
            marginRight: 16,
            display: 'flex',
            alignItems: 'center',
          }}
        >
          <Tooltip title={t('listPage.date.creator')}>
            <DefaultText>{data.feature.creator.login}</DefaultText>
          </Tooltip>
          {data.feature.creator.isVerified && <StarIcon />}
        </BluredBlock>
        <Tooltip title={t('listPage.date.recentEarnings')}>
          <BluredBlock
            style={{
              marginRight: 16,
              display: 'flex',
              alignItems: 'center',
            }}
          >
            <DefaultText
              style={{
                marginRight: 10,
              }}
            >
              {t('listPage.date.halfYearAgo')}
            </DefaultText>
            <AccentText danger={!data.feature.averageReturn.isIncreased}>
              {data.feature.averageReturn.value}%
            </AccentText>
          </BluredBlock>
        </Tooltip>
      </Line>
      <Line>
        <Tooltip title={t('listPage.date.averageMoney')}>
          <BluredBlock
            style={{
              marginRight: 10,
              display: 'flex',
              alignItems: 'center',
              marginBottom: 0,
            }}
          >
            <TransactionIcon />
            <PriceText>2 379 ETH</PriceText>
          </BluredBlock>
        </Tooltip> */}
      </Line>
      {/* <ChartWrapper ref={chartContainer} /> */}
    </Wrapper>
  );
};
