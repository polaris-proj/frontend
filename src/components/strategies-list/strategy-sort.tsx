import React, { FC } from 'react';
import { Form, Select } from 'antd';
import { InputItem } from './strategy-filter';
import { TFunction } from 'react-i18next';

const { Option } = Select;

export interface IStrategySort {
  popular?: string;
  earning?: string;
  absoluteValue?: string;
}

interface Props {
  t: TFunction<string[]>;
}

export const StrategySort: FC<Props> = ({ t }) => {
  const [form] = Form.useForm<IStrategySort>();

  return (
    <Form<IStrategySort>
      form={form}
      style={{
        display: 'flex',
        flexWrap: 'wrap',
        marginBottom: '4px',
      }}
    >
      <InputItem>
        <Select
          placeholder={t('listPage.filters.form.popularSortPlaceholder')}
          style={{
            width: 200,
          }}
          allowClear
        >
          <Option value={'most-popular'}>
            {t('listPage.filters.form.popularMost')}
          </Option>
          <Option value={'most-unpopular'}>
            {t('listPage.filters.form.popularMin')}
          </Option>
        </Select>
      </InputItem>
      <InputItem>
        <Select
          placeholder={t('listPage.filters.form.profitPlaceholder')}
          style={{
            width: 190,
          }}
          allowClear
        >
          <Option value={'most-profit'}>
            {t('listPage.filters.form.profitMost')}
          </Option>
          <Option value={'most-unprofit'}>
            {t('listPage.filters.form.profitMin')}
          </Option>
        </Select>
      </InputItem>
      <InputItem>
        <Select
          placeholder={t('listPage.filters.form.usualSortPlaceholder')}
          style={{
            width: 190,
          }}
          allowClear
        >
          <Option value={'most-profit'}>
            {t('listPage.filters.form.usualMost')}
          </Option>
          <Option value={'most-unprofit'}>
            {t('listPage.filters.form.usualMin')}
          </Option>
        </Select>
      </InputItem>
    </Form>
  );
};
