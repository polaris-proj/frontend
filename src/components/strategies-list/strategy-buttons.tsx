import React, { FC } from 'react';
import styled from '@emotion/styled';
import { Button } from 'antd';
import { TFunction } from 'react-i18next';

const Wrapper = styled('div')`
  display: flex;
  flex-wrap: wrap;
  padding-top: 4px;
`;

const ButtonWrapper = styled('div')`
  margin-right: 16px;
`;

interface Props {
  t: TFunction<string[]>;
}

export const StrategyButtons: FC<Props> = ({ t }) => {
  return (
    <Wrapper>
      <ButtonWrapper>
        <Button type={'primary'}>{t('listPage.filters.buttons.accept')}</Button>
      </ButtonWrapper>
      <ButtonWrapper>
        <Button type={'default'}>{t('listPage.filters.buttons.reset')}</Button>
      </ButtonWrapper>
    </Wrapper>
  );
};
