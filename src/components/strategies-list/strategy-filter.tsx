import React, { FC, useCallback } from 'react';
import styled from '@emotion/styled';
import { Form, Input, InputNumber, Select } from 'antd';
import {
  SecurityScanOutlined,
  StockOutlined,
  TransactionOutlined,
} from '@ant-design/icons';
import { TFunction } from 'react-i18next';

const { Option } = Select;

export const InputItem = styled(Form.Item)`
  margin-bottom: 12px;
  margin-right: 16px;
`;

export interface IStrategyFilter {
  name?: string;
  currency?: string;
  author?: string;
  usualValue?: number;
  tags?: string[];
}

interface Props {
  t: TFunction<string[]>;
}

export const StrategyFilter: FC<Props> = ({ t }) => {
  const [form] = Form.useForm<IStrategyFilter>();

  const handleSubmit = useCallback((data: IStrategyFilter) => {
    console.log(data);
  }, []);

  return (
    <Form<IStrategyFilter>
      style={{
        display: 'flex',
        flexWrap: 'wrap',
        marginBottom: '2px',
      }}
      form={form}
      onFinish={handleSubmit}
    >
      <InputItem name={'name'}>
        <Input
          prefix={<SecurityScanOutlined />}
          placeholder={t('listPage.filters.form.namePlaceholder')}
          style={{
            width: 260,
          }}
          allowClear
        />
      </InputItem>
      <InputItem name={'currency'}>
        <Select
          placeholder={t('listPage.filters.form.currencyPlaceholder')}
          style={{
            width: 120,
          }}
          allowClear
        >
          <Option value={'usdt'}>USDT</Option>
          <Option value={'btc'}>BTC</Option>
          <Option value={'eth'}>ETH</Option>
        </Select>
      </InputItem>
      <InputItem name={'author'}>
        <Select
          placeholder={t('listPage.filters.form.authorPlaceholder')}
          style={{
            width: 200,
          }}
          allowClear
        >
          <Option value={'pp'}>Polaris Project</Option>
          <Option value={'some'}>Some user</Option>
        </Select>
      </InputItem>
      <InputItem>
        <Input.Group
          compact
          style={{
            display: 'flex',
          }}
        >
          <InputNumber
            style={{
              width: 160,
              display: 'flex',
            }}
            name={'minValue'}
            min={-100}
            max={100}
            placeholder={t('listPage.filters.form.minValuePlaceholder')}
            prefix={<StockOutlined />}
          />
          <InputNumber
            style={{
              width: 140,
            }}
            name={'maxValue'}
            min={-100}
            max={100}
            placeholder={t('listPage.filters.form.maxValuePlaceholder')}
          />
        </Input.Group>
      </InputItem>
      <InputItem name={'usualValue'}>
        <InputNumber
          style={{
            width: 160,
            display: 'flex',
          }}
          min={0}
          max={99999999999999}
          placeholder={t('listPage.filters.form.usualValuePlaceholder')}
          prefix={<TransactionOutlined />}
        />
      </InputItem>
      <InputItem name={'tags'}>
        <Select
          placeholder={t('listPage.filters.form.tagsPlaceholder')}
          style={{
            width: 200,
          }}
          allowClear
          mode={'multiple'}
          maxTagCount={'responsive'}
        >
          <Option value={'pp'}>Polaris Project</Option>
          <Option value={'some'}>Some user</Option>
        </Select>
      </InputItem>
    </Form>
  );
};
