import React, { FC, ReactNode } from 'react';
import styled from '@emotion/styled';
import { Header, HeaderStyles } from '../layout/header';
import { Menu } from '../layout/menu';
import { SideMenu, SideMenuSettings } from '../layout/side-menu';
import { MenuSettings } from '../layout/menu-item';
import { useTranslation } from 'react-i18next';
import { LocaleKeys } from '~/locale';

const AppContainer = styled('div')`
    height: 100vh;
    width: calc(100vw - 120px - 24px);
    overflow-x: hidden;
    overflow-y: auto;
    max-width: 100vw;
    max-height: 100vh;
    font-family: 'Open Sans', sans-serif;
    padding-top: ${HeaderStyles.height};
    padding-left: ${MenuSettings.width};
    padding-right: ${SideMenuSettings.width};
    background-color: ${({ theme }) => theme.COLORS.WHITE.C300};
    transition: background-color 0.3s ease-out;
    color: ${({ theme }) => theme.COLORS.TEXT.COMMON_TEXT};
    display: flex;
`;

const ContentContainer = styled('div')`
    width: calc(100vw - 60px - 60px - 12px - 12px);
`;

interface Props {
  children: ReactNode;
}

export const MainLayout: FC<Props> = ({ children }) => {
  const { t } = useTranslation([LocaleKeys.LAYOUT]);
  return (
    <AppContainer>
        <Header t={t} />
          <Menu t={t} />
          <SideMenu />
          <ContentContainer>
            {children}
          </ContentContainer>
    </AppContainer>
  );
};
