import React, { FC, PropsWithChildren, useMemo } from 'react';
import { NavLink, useLocation } from 'react-router-dom';
import { Paths } from '../../utils/routes/paths';
import { HomeOutlined } from '@ant-design/icons';
import { Breadcrumb } from 'antd';
import { useTranslation } from 'react-i18next';
import { LocaleKeys } from '../../locale';
import styled from '@emotion/styled';
import { MainMenuRoutes } from '../../utils/routes/menu-routes';

interface IBreadCrumb {
  to: string;
  text: string;
  Icon?: typeof HomeOutlined;
}

const WithIconWrapper = styled('div')<{ isactive?: 'true' | 'false' }>`
  display: inline-flex;
  align-items: center;
  gap: 4px;

  span[role='img'] {
    transition: color 0.3s ease-out;
  }

  :hover {
    span[role='img'] {
      color: ${({ theme, isactive }) =>
        isactive === 'true'
          ? theme.COLORS.TEXT.HEADING
          : theme.COLORS.TEXT.PRIMARY} !important;
    }
  }
`;

const BreadItem = styled(Breadcrumb.Item)<{ isactive?: 'true' | 'false' }>`
  color: ${({ theme, isactive }) =>
    isactive === 'true'
      ? theme.COLORS.TEXT.HEADING
      : theme.COLORS.TEXT.SECONDARY} !important;

  a {
    color: ${({ theme }) => theme.COLORS.TEXT.SECONDARY} !important;

    &:hover {
      color: ${({ theme }) => theme.COLORS.TEXT.PRIMARY} !important;
    }
  }
`;

const Wrapper = styled('div')<{ isFullHeight: boolean }>`
  height: ${({ isFullHeight }) => (isFullHeight ? '100%' : 'auto')};
`;

interface Props {
  fullHeight?: boolean;
}

export const WithBreadCrumbs: FC<PropsWithChildren<Props>> = ({
  children,
  fullHeight,
}) => {
  const location = useLocation();

  const { t } = useTranslation([LocaleKeys.PATHS]);

  const breadCrumbs = useMemo(() => {
    const result: IBreadCrumb[] = [];

    const pathsValues: string[] = Object.values(Paths);

    location.pathname.split('/').forEach((el) => {
      const icon = MainMenuRoutes(t).find(
        (menuEl) =>
          !!menuEl.to.find((pathEl) => pathEl.path === (`/${el}` as Paths))
      )?.Icon;

      if (el === '') {
        result.push({
          to: Paths.BASE,
          text: t(Paths.BASE),
          Icon: icon,
        });
      } else if (pathsValues.indexOf(`/${el}`) > -1) {
        result.push({
          to: `/${el}`,
          text: t(`/${el}`),
          Icon: icon,
        });
      } else {
        result.push({
          to: location.pathname,
          text: t(`#${el}`),
          Icon: icon,
        });
      }
    });

    return result;
  }, [location.pathname, t]);

  return (
    <Wrapper isFullHeight={!!fullHeight}>
      {breadCrumbs.length > 1 && (
        <Breadcrumb>
          {breadCrumbs.map(({ text, Icon, to }) => (
            <BreadItem
              key={text}
              isactive={String(location.pathname === to) as 'true' | 'false'}
            >
              {location.pathname === to ? (
                <WithIconWrapper
                  isactive={
                    String(location.pathname === to) as 'true' | 'false'
                  }
                >
                  {Icon && <Icon />}
                  <span>{text}</span>
                </WithIconWrapper>
              ) : (
                <NavLink to={to}>
                  <WithIconWrapper
                    isactive={
                      String(location.pathname === to) as 'true' | 'false'
                    }
                  >
                    {Icon && <Icon />}
                    <span>{text}</span>
                  </WithIconWrapper>
                </NavLink>
              )}
            </BreadItem>
          ))}
        </Breadcrumb>
      )}
      <div
        style={{
          marginTop: 20,
          paddingBottom: 40,
        }}
      >
        {children}
      </div>
    </Wrapper>
  );
};
