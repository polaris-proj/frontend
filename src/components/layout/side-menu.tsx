import React, { FC } from 'react';
import styled from '@emotion/styled';
import { HeaderStyles } from './header';
import { ZIndex } from '~styles/z-index';
import { BlockShadow } from '~styles/mixins';
import { MenuSettings } from './menu-item';

export const SideMenuSettings = {
  width: '60px',
};

const SideMenuWrapper = BlockShadow({
  target: styled('div')`
    top: ${HeaderStyles.height};
    right: 0;
    position: fixed;
    height: calc(${MenuSettings.height} - ${HeaderStyles.height});
    width: ${MenuSettings.width};
    z-index: ${ZIndex.Z6};
    background-color: ${({ theme }) => theme.COLORS.WHITE.C100};
    transition: background-color 0.3s ease-out;
  `,
});

export const SideMenu: FC = () => {
  return <SideMenuWrapper />;
};
