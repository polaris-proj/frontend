import {useAuthSelector} from "~ducks/auth/authSelectors";
import {Paths} from "~utils/routes/paths";
import {UserMainCard} from "~components/common/UserMainCard";
import React from "react";
import styled from "@emotion/styled";
import {NavLink} from "react-router-dom";
import {StartButton, StyledButton} from "~components/common/buttons";



const UserWrapper = styled(NavLink)`
  padding-left: 62px;
`;

export const UserProfile = () => {
  const t = useAuthSelector();

  if(t.authed){
    return(
      <UserWrapper to={Paths.ACCOUNT}>
        <UserMainCard />
      </UserWrapper>
    );
  }

  return(
    <UserWrapper to={Paths.LOGIN}>
      <StyledButton type={'primary'} >Login</StyledButton>
    </UserWrapper>
  );
}
