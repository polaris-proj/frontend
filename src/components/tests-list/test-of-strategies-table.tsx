import React, { FC } from 'react';

import { useNavigate } from 'react-router-dom';
import styled from '@emotion/styled';
import { Theme } from '@emotion/react';


import { Table } from 'antd';
import type { ColumnsType } from 'antd/es/table';
import { Paths } from '~utils/routes/paths';
import { TestResults } from '../common/testResult';
import {TestResult} from "~apiClients/DataTypes/TestResult";
import {DateTimeRange} from "~apiClients/DataTypes/DateTimeRange";

interface Props {
  data: TestResult[];
}

const Wrapper = styled('div')`
  display: flex;
  flex-wrap: wrap;
  padding-left: 20px;
  padding-top: 4px;
`;


const getPrintableDateRange = (dateRange: DateTimeRange) => {
  return `${dateRange.begin} - ${dateRange.end}`;
}
export const TestsTable: FC<Props> = ({data }) => {

  const columns: ColumnsType<TestResult> = [
    {
      title: "ID",
      dataIndex: 'runId',
      key: 'runId',
      render: (id: string) => <span>{id}</span>,
    },
    {
      title: "Period",
      dataIndex: ['startTime', 'endTime'],
      key: 'period',
      render: (_: any, record: TestResult) => <span>{getPrintableDateRange(record.duration)}</span>
    },
    {
      title: "Profit",
      dataIndex: ['absEarning', 'relEarning'  ],
        key: 'profit',
      render: (_: any, record: TestResult) => <TestResults test={record} />,
    }
  ];

  const navigate = useNavigate();

  return <>
    <Wrapper>
      <Table
        title={(_: any) => "Результаты тестирования"}
        scroll={{ x: 1300 }}
        rowKey="runId"
        bordered={true}
        columns={columns}
        dataSource={data}
        onRow={(record: TestResult, _) => {
          return {
            onClick: event => navigate(`${Paths.RUNS}/${record.runId}`)
          };
        }}
      />
    </Wrapper>
  </>
}
