import { Theme } from "@emotion/react";
import { ConfigProvider } from "antd";
import React, { ReactNode } from "react";
import { FC } from "react";


interface Props {
    theme: Theme;
    children: ReactNode;
  }

export const AntConfig: FC<Props> = ({theme, children}) => {
    return (
        <ConfigProvider
        theme={{
          token: {
            colorBgBase: theme.COLORS.WHITE.C100,
            colorText: theme.COLORS.TEXT.COMMON_TEXT,
            colorPrimary: theme.COLORS.ACCENT.PRIMARY,
            colorSuccess: theme.COLORS.ACCENT.SUCCESS,
            colorError: theme.COLORS.ACCENT.DANGER,
            colorWarning: theme.COLORS.ACCENT.WARNING,
          }
        }}
        >
            {children}
        </ConfigProvider>
    );
  };
  