import React, { FC } from 'react';
import styled from '@emotion/styled';

const Wrapper = styled('div')<{ size: number }>`
  width: ${(props) => props.size}px;
  height: ${(props) => props.size}px;
  border-radius: 50%;
  overflow: hidden;
`;

const Image = styled('div')<{ source: string }>`
  background-image: url('${(props) => props.source}');
  width: 100%;
  height: 100%;
  background-size: cover;
`;

interface Props {
  source: string;
  size: number;
}

export const UserAvatar: FC<Props> = ({ source, size }) => {
  return (
    <Wrapper size={size}>
      <Image source={source} />
    </Wrapper>
  );
};
