import React, { FC } from 'react';
import { Descriptions, DescriptionsProps, Space, Typography } from 'antd';
import styled from '@emotion/styled';
import { TFunction } from 'react-i18next';
import { IParam } from '../../types/strategies/strategy';

interface Props {
  data?: IParam[];
  t: TFunction<string[]>;
}

const Wrapper = styled(Space)`
  margin-top: 30px;
`;

export const StrategyParamsDesciption: FC<Props> = ({ data, t }) => {
  if (!data) {
    return null;
  }
  const items: DescriptionsProps['items'] = data.map((param, i) => {
      return {
        key: i,
        label: param.name,
        children: param.value
      };
  });

  return (
    <Wrapper direction={'vertical'} size={'middle'}>
      <Descriptions column={1} title={t('currentPage.common.paramsTitle')} items={items} />
      { data.length == 0 ? t('currentPage.common.noParams') : ""}  
    </Wrapper>
  );
};
