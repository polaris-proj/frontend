import styled from '@emotion/styled';
import { Sizes } from '../../styles';

export const BaseContentWrapper = styled('div')`
  border-radius: ${Sizes.BORDER_RADIUS_SM};
  background-color: ${({ theme }) => theme.COLORS.WHITE.C100};
  padding: 20px;
  transition: background-color 0.3s ease-out;
`;
