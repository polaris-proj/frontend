import React, { FC } from 'react';
import { StyledPrice } from './price';
import {TestResult} from "~apiClients/DataTypes/TestResult";

interface Props {
    test: TestResult;
}

export const TestResults: FC<Props> = ({ test }) => {
    return (
        <span><StyledPrice danger={test.profit < 0}>{test.profit}</StyledPrice></span>
    );
};
