import { Button, ButtonProps } from 'antd';
import styled from '@emotion/styled';
import React, { FC } from 'react';
import { useCurrentTheme } from '~utils/services/ThemeService';

const Container = styled('div')`
    display: flex;
    border: 1px ${({ theme }) => theme.COLORS.ACCENT.PRIMARY} solid;
    border-radius: 2px;
`

export const StyledButton = styled(Button)`
    border-radius: 2px;
`;

export const StartButton: FC<ButtonProps> = (props: ButtonProps) => {
    const theme = useCurrentTheme();
    return(
        <Container>
            <StyledButton type={'primary'} {...props} />
        </Container>
    );
};
