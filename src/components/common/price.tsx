import styled from "@emotion/styled";
import { ICurrency } from "../../types/common/currency";
import { FC } from "react";
import React from "react";
import { currencyToText } from "../../utils/functions/amount";

export const StyledPrice = styled('span')<{ danger?: boolean }>`
  color: ${({ theme, danger }) =>
    danger ? theme.COLORS.ACCENT.DANGER : theme.COLORS.ACCENT.SUCCESS};
  font-weight: 700;
  font-size: 14px;
  line-height: 22px;
`;

interface Props {
  currency: ICurrency;
}

export const Price: FC<Props> = ({ currency }) => {
  return (
    <StyledPrice danger={currency.amount < 0}>
      {currencyToText(currency)}
    </StyledPrice>
  );
};
