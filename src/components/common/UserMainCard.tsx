import React, {FC, useEffect, useState} from 'react';
import styled from '@emotion/styled';
import { UserAvatar } from './userAvatar';
import { ArrowDown } from '../icons/ArrowDown';

import MockMainUser from '../../mocks/images/main-user.jpg';
import {useAuthSelector} from "~ducks/auth/authSelectors";
import {getUserDataAction} from "~ducks/user/actions";
import {User} from "~apiClients/DataTypes/User";
import {useDispatch} from "react-redux";
import {useUserDataSelector} from "~ducks/user/user-data/userDataSlice";

const Wrapper = styled('div')`
  display: flex;
  gap: 10px;
  cursor: pointer;
`;

const InfoWrapper = styled('div')`
  display: flex;
  align-items: center;
  gap: 10px;
`;

const InfoInner = styled('div')`
  padding-top: 10px;
  display: flex;
  flex-direction: column;
  gap: -3px;
`;

const Name = styled('span')`
  font-weight: 700;
  font-size: 12px;
  line-height: 16px;
  color: ${({ theme }) => theme.COLORS.TEXT.COMMON_TEXT};
  transition: color 0.3s ease-out;
`;

const Status = styled('span')`
  font-weight: 400;
  font-size: 10px;
  line-height: 14px;
  color: ${({ theme }) => theme.COLORS.TEXT.DISABLED};
  transition: color 0.3s ease-out;
`;

export const UserMainCard: FC = () => {

  const t = useUserDataSelector();
  const [user, setUser] = useState<Nullable<User>>(null);

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUserDataAction);
      setUser(t.data);
  }, [dispatch, t]);

  return (
    <Wrapper>
      <UserAvatar size={42} source={MockMainUser} />
      <InfoWrapper>
        <InfoInner>
          <Name>{user?.login}</Name>
          <Status>Деньги...</Status>
        </InfoInner>
      </InfoWrapper>
    </Wrapper>
  );
};
