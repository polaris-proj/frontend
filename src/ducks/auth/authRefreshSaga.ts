import { call, put, takeLeading } from 'redux-saga/effects';
import { handleLogout, refreshAction, setAuth } from './authSlice';
import { accessTokenPath, refreshTokenPath } from '~utils/common/localStoragePaths';
import { message } from 'antd';
import { authApi } from "~apiClients/hooks/useApi";

function* refreshWorker(): Generator {
  const token = localStorage.getItem(refreshTokenPath);
  if (token) {
    const loading = message.loading('Попытка повторной авторизации...');

    try {
      const response: any = yield authApi.refreshToken(token);
      localStorage.setItem(accessTokenPath, response.accessToken);
      localStorage.setItem(refreshTokenPath, response.refreshToken);
      yield put(setAuth(response));
      yield call(loading);
    } catch (e: any) {
      yield call(loading);
      yield put(handleLogout());
      yield call(
        message.warning,
        'Попытка авторизации не удалась, войдите заново в аккаунт'
      );
    }
  } else {
    yield put(handleLogout());
  }
}

export function* authRefreshSaga(): Generator {
  yield takeLeading(refreshAction, refreshWorker);
}
