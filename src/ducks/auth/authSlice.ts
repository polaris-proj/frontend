import { createAction, createSlice, PayloadAction } from '@reduxjs/toolkit';
import type {LoginResult} from "~apiClients/DataTypes/LoginResult";

export interface ILoginBaseError {
  status: number;
}

export const loginAction = createAction<{
  login: string;
  password: string;
  loadingCallBack: () => void;
}>('user/auth/login');

export const refreshAction = createAction('user/auth/refresh');

type InitialType =
  | {
      authed: false;
      data: null;
      error: ILoginBaseError & {
        isError: boolean;
      };
    }
  | {
      authed: true;
      data: LoginResult;
      error: ILoginBaseError & {
        isError: boolean;
      };
    };

const initialState: InitialType = {
  authed: false,
  data: null,
  error: {
    isError: false,
    status: -1,
  },
};

const userAuthSlice = createSlice({
  name: 'user/auth',
  initialState: initialState as InitialType,
  reducers: {
    setAuth(state, action: PayloadAction<LoginResult>) {
      console.log(action)
      state.data = action.payload;
      state.authed = true;
      console.log(state)
    },
    setAuthError(state, action: PayloadAction<ILoginBaseError>) {
      state.authed = false;
      state.error = {
        isError: true,
        status: action.payload.status,
      };
    },
    clearError(state) {
      state.error = {
        isError: false,
        status: -1,
      };
    },
    handleLogout(state) {
      console.log('logout')
      state.authed = false;
      state.error = {
        isError: false,
        status: -1,
      };
      state.data = null;
    },
  },
});

export const { setAuth, setAuthError, clearError, handleLogout } =
  userAuthSlice.actions;
export const userAuthReducer = userAuthSlice.reducer;
