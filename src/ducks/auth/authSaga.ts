import { call, put, takeLeading } from 'redux-saga/effects';
import { loginAction, setAuth, setAuthError } from './authSlice';
import { accessTokenPath,  refreshTokenPath } from '~utils/common/localStoragePaths';
import { reset } from '../strategies/sagas/resetSaga'
import { authApi } from "~apiClients/hooks/useApi";
import type {LoginResult} from "~apiClients/DataTypes/LoginResult";

function* loginWorker(action: ReturnType<typeof loginAction>): Generator {
  try {
    const response: any = yield call(authApi.login.bind(authApi), {
      login: action.payload.login,
      password: action.payload.password,
    });

    localStorage.setItem(accessTokenPath, response.accessToken);
    localStorage.setItem(refreshTokenPath, response.refreshToken);

    yield put(setAuth(response));
    yield call(action.payload.loadingCallBack);

    yield call(reset, {});
  } catch (e: unknown) {
    console.log(e);
    yield call(action.payload.loadingCallBack);
    yield put(setAuthError({ status: e.response?.status || "server didn't respond" }));
  }
}

export function* authSaga(): Generator {
  yield takeLeading(loginAction, loginWorker);
}
