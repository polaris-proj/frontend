import { useAppSelector } from '~/ducks';

export const useAuthSelector = () => useAppSelector((state) => state.userAuth);
