import { combineReducers } from '@reduxjs/toolkit';
import { runsListReducer } from './slices/runListSlice';
import { runReducer } from './slices/runSlice';

export const strategyRunReducers = combineReducers({
  list: runsListReducer,
  current: runReducer,
});
