import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { useAppSelector } from '../../index';
import { IRun } from '../../../types/strategies/run';

interface IInitialState {
  loading: boolean;
  loaded: boolean;
  errorOnLoad: boolean;
  data: IRun[];
}

const initialState: IInitialState = {
  loading: false,
  loaded: false,
  errorOnLoad: false,
  data: [],
};

const runsListSlice = createSlice({
  name: 'run/list',
  initialState,
  reducers: {
    setLoading(state, action: PayloadAction<boolean>) {
      state.loading = action.payload;
    },
    setLoaded(
      state,
      action: PayloadAction<{
        loaded: boolean;
        errorOnLoad: boolean;
      }>
    ) {
      state.loaded = action.payload.loaded;
      state.loading = false;
      state.errorOnLoad = action.payload.errorOnLoad;
    },
    setData(state, action: PayloadAction<IRun[]>) {
    //   state.data = action.payload;
    },
  },
});

export const { setLoading, setLoaded, setData } = runsListSlice.actions;
export const runsListReducer = runsListSlice.reducer;
export const useRunsListSelector = () =>
  useAppSelector((state) => state.runs.list);
