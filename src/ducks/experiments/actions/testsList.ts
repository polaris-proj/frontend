import { createAction } from '@reduxjs/toolkit';

export const fetchTestsListAction = createAction<{
    id: string;
}>('test/load/all');
