import { createAction } from '@reduxjs/toolkit';

export const fetchRunAction = createAction<{
  id: string;
}>('run/load');
