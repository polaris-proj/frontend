import { createAction } from '@reduxjs/toolkit';

export const fetchTestAction = createAction<{
  id: string;
}>('test/load');
