import { createAction } from '@reduxjs/toolkit';
import type {User} from "~apiClients/DataTypes/User";

function withPayloadType<T>() {
  return (t: T) => ({ payload: t })
}

export const getUserDataAction = createAction('user/data/get');
