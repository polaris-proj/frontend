import { call, put, takeLeading } from 'redux-saga/effects';
import { getUserDataAction } from '../actions';
import { setData, setError, setLoading } from './userDataSlice';
import { setBaseContainerLoading } from '../../application/services/loadingSlice';
import { refreshAction } from '../../auth/authSlice';
import { userApi } from "~apiClients/hooks/useApi";
import { HttpError } from "~apiClients/ApiBase/HttpError";

function* userDataWorker(
  action: ReturnType<typeof getUserDataAction>
): Generator {
  try {
    yield put(setLoading(true));
    yield put(setBaseContainerLoading(true));
    const response: any = yield call(userApi.getUserInfo.bind(userApi));
    yield put(setData(response));
    yield put(setBaseContainerLoading(false));
    yield put(setLoading(false));
  } catch (e: any) {
    if(e instanceof HttpError) {
      if (e.status === 401) {
        yield put(refreshAction());
        yield put(action);
      }
    }
    yield put(setLoading(false));
    yield put(setError(false));
  }
}

export function* userDataSaga(): Generator {
  yield takeLeading(getUserDataAction, userDataWorker);
}
