import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { User } from '~/apiClients/DataTypes/User';
import { useAppSelector } from '~/ducks';

type IInitialState = {
  data: User | null;
  loaded: boolean;
  loading: boolean;
  errorOnLoad: boolean;
};

const initialState: IInitialState = {
  data: null,
  loaded: false,
  errorOnLoad: false,
  loading: false,
};

const userDataSlice = createSlice({
  name: 'user/data',
  initialState,
  reducers: {
    setData(state, action: PayloadAction<User>) {
      state.data = action.payload;
      state.loaded = true;
      state.errorOnLoad = false;
    },
    setLoaded(state, action: PayloadAction<boolean>) {
      state.loaded = action.payload;
    },
    setLoading(state, action: PayloadAction<boolean>) {
      state.loading = action.payload;
    },
    setError(state, action: PayloadAction<boolean>) {
      state.errorOnLoad = action.payload;
    },
  },
});

export const { setData, setLoading, setError } = userDataSlice.actions;
export const userDataReducer = userDataSlice.reducer;

export const useUserDataSelector = () =>
  useAppSelector((state) => state.user.data);
