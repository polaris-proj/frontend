import { combineReducers } from '@reduxjs/toolkit';
import { userDataReducer } from './user-data/userDataSlice';

export const userReducers = combineReducers({
  data: userDataReducer,
});
