import { combineReducers, Reducer } from '@reduxjs/toolkit';
import { applicationReducers } from './application';
import { userAuthReducer } from './auth/authSlice';
import storage from 'redux-persist/lib/storage';
import { persistReducer } from 'redux-persist';
import { userReducers } from './user';
import { strategiesReducers } from './strategies';

const userAuthConfig = {
  key: 'user/auth',
  storage,
};

const appReducer = combineReducers({
  application: applicationReducers,
  userAuth: persistReducer(userAuthConfig, userAuthReducer),
  user: userReducers,
  strategies: strategiesReducers,
});

export type RootCombine = ReturnType<typeof appReducer>;

export const rootReducer: Reducer = (state: RootCombine, action) => {
  if (action.type === 'auth/logOut') {
    /**
     * Возможен сброс store при logout
     */
    //
  }

  return appReducer(state, action);
};
