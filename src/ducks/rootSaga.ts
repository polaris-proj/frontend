import { all, call, put, spawn } from 'redux-saga/effects';
import { message } from 'antd';
import { authSaga } from './auth/authSaga';
import { userDataSaga } from './user/user-data/userDataSaga';
import { authRefreshSaga } from './auth/authRefreshSaga';
import { fetchStrategyListSaga } from './strategies/sagas/fetchStrategyListSaga';
import { fetchStrategySaga } from './strategies/sagas/fetchStrategySaga';
import { fetchStrategyParamsSaga } from './strategies/sagas/fetchStrategyParamsSaga';

function* rootSaga(): Generator {
  const sagas = [
    authSaga,
    userDataSaga,
    authRefreshSaga,
    fetchStrategyListSaga,
    fetchStrategySaga,
    fetchStrategyParamsSaga,
  ];

  yield all(
    sagas.map((saga) =>
      spawn(function* () {
        while (true) {
          try {
            yield call(saga);
            break;
          } catch (e: any) {
            console.log(e);
            if (e.response.status === 401) {
              yield put({ type: 'reset/token' });
            } else {
              message.error(e.response.data.message).then();
            }
          }
        }
      })
    )
  );
}

export default rootSaga;
