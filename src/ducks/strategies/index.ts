import { combineReducers } from '@reduxjs/toolkit';
import { strategyListReducer } from './slices/strategyListSlice';
import { strategyReducer } from './slices/strategySlice';
import { strategyParamsReducer } from './slices/strategyParamsSlice';

export const strategiesReducers = combineReducers({
  list: strategyListReducer,
  current: strategyReducer,
  params: strategyParamsReducer,
});
