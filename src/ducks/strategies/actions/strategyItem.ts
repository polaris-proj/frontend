import { createAction } from '@reduxjs/toolkit';
import { IExchangedParamsItem } from '../slices/exchangeParamsSlice';

export const fetchStrategyAction = createAction<{
  id: string;
}>('strategy/load');
export const fetchStrategyParamsAction = createAction<{
  id: string;
  callBack: () => void;
  errorCallBack: () => void;
}>('strategy/params/load');
export const startStrategyAction = createAction<{
  id: string;
  startTime: string;
  endTime: string;
  callBack?: () => void;
  errorCallBack: () => void;
  params: IExchangedParamsItem[];
}>('strategy/start');
export const fetchStrategyExchangeParamsAction = createAction(
  'strategy/exchange-params/get'
);
