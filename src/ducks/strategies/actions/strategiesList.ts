import { createAction } from '@reduxjs/toolkit';

export const fetchStrategiesList = createAction('strategies/load/all');
