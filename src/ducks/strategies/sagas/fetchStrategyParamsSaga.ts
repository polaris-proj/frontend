import { call, put, takeLeading } from 'redux-saga/effects';
import { message } from 'antd';
import { fetchStrategyParamsAction } from '../actions/strategyItem';
import { setData, setLoading, setError } from '../slices/strategyParamsSlice';
import { refreshAction } from '../../auth/authSlice';
import {strategyApi} from "~apiClients/hooks/useApi";

function* loadSagaWorker(
  action: ReturnType<typeof fetchStrategyParamsAction>
): Generator {
  try {
    yield put(
      setLoading({
        id: action.payload.id,
        status: true,
      })
    );
    const response: any = yield call(
      strategyApi.getStrategyParameters.bind(strategyApi),
      action.payload.id
    );
    yield put(
      setData({
        id: action.payload.id,
        data: response.data,
      })
    );
    yield call(action.payload.callBack);
  } catch (e: any) {
    yield put(
      setError({
        id: action.payload.id,
        error: true,
      })
    );
    if (action.payload.errorCallBack) {
      yield call(action.payload.errorCallBack);
    }
    if (e.response.status === 401) {
      try {
        yield put(refreshAction());
        yield put(action);
      } catch (e) {}
    } else {
      yield call(message.error, e.response.status);
    }
  }
}

export function* fetchStrategyParamsSaga(): Generator {
  yield takeLeading(fetchStrategyParamsAction, loadSagaWorker);
}
