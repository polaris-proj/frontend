import { put, call, takeEvery } from 'redux-saga/effects';
import { setData, setLoaded, setLoading } from '../slices/strategySlice';
import { fetchStrategyAction } from '../actions/strategyItem';
import {strategyApi} from "~apiClients/hooks/useApi";

export function* fetchStrategy(strategyId: string) : Generator {
  yield put(setLoading(true));
  const strategy: any = yield call(strategyApi.getStrategyParameters.bind(strategyApi), strategyId);
  yield put(setLoading(false));
  if (strategy) {
    yield put(setData(strategy));
    yield put(setLoaded({
      loaded: true,
      errorOnLoad: false,
    }));
  } else {
    yield put(setLoaded({
      loaded: true,
      errorOnLoad: true,
    }));
  }
  return;
}

function* loadSagaWorker(
  action: ReturnType<typeof fetchStrategyAction>
): Generator {
  yield put(setLoading(true));

  let strategy: any;
  try {
    strategy = yield fetchStrategy(action.payload.id);
    //yield updateTestsList(strategy.id);
  } catch (e) {
    yield put(setLoading(false));
    return;
  }


  if (strategy) {
    yield put(setData(strategy));
    yield put(setLoaded({
      loaded: true,
      errorOnLoad: false,
    }));
  } else {
    yield put(setLoaded({
      loaded: true,
      errorOnLoad: true,
    }));
  }
}

export function* fetchStrategySaga(): Generator {
  yield takeEvery(fetchStrategyAction, loadSagaWorker);
}
