import { call, put, takeLeading } from 'redux-saga/effects';
import { fetchStrategiesList } from '../actions/strategiesList';
import { baseAPI } from '../../../services';
import { setData, setLoaded, setLoading } from '../slices/strategyListSlice';
import { message } from 'antd';
import { refreshAction } from '../../auth/authSlice';
import {strategyApi} from "~apiClients/hooks/useApi";

export function* fetchStrategiesListWorker(
  action: ReturnType<typeof fetchStrategiesList>
): Generator {
  try {
    yield put(setLoading(true));
    const response: any = yield call(strategyApi.getStrategyList.bind(strategyApi));
    yield put(setData(response));
    yield put(
      setLoaded({
        loaded: true,
        errorOnLoad: false,
      })
    );
  } catch (e: any) {
    console.log(e);
    yield put(
      setLoaded({
        loaded: false,
        errorOnLoad: true,
      })
    );
  }
}

export function* fetchStrategyListSaga(): Generator {
  yield takeLeading(fetchStrategiesList, fetchStrategiesListWorker);
}
