import { put } from 'redux-saga/effects';
import * as list from '../slices/strategyListSlice';
import * as single from '../slices/strategySlice';

export function* reset(
  action: any
): Generator {
  yield put(list.reset({}));
  yield put(single.reset({}));
}

