import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { useAppSelector } from '../../index';
import { IStrategy } from '../../../types/strategies/strategy';
import type {ParameterModel} from "~apiClients/DataTypes/ParameterModel";

export interface IInitialState {
  loading: boolean;
  loaded: boolean;
  errorOnLoad: boolean;
  data: ParameterModel[];
}

const initialState: IInitialState = {
  loading: false,
  loaded: false,
  errorOnLoad: false,
  data: [],
};

const strategySlice = createSlice({
  name: 'strategy/current',
  initialState,
  reducers: {
    setLoading(state, action: PayloadAction<boolean>) {
      state.loading = action.payload;
    },
    setLoaded(
      state,
      action: PayloadAction<{
        loaded: boolean;
        errorOnLoad: boolean;
      }>
    ) {
      state.loaded = action.payload.loaded;
      state.loading = false;
      state.errorOnLoad = action.payload.errorOnLoad;
    },
    setData(state, action: PayloadAction<ParameterModel[]>) {
      state.data = action.payload;
    },
    reset(state, PayloadAction) {
      state.loading = initialState.loading;
      state.loaded = initialState.loaded;
      state.errorOnLoad = initialState.errorOnLoad;
      state.data = initialState.data;
    }
  },
});

export const { setLoading, setLoaded, setData, reset } = strategySlice.actions;
export const strategyReducer = strategySlice.reducer;
export const useStrategyCurrentSelector = () =>
  useAppSelector((state) => state.strategies.current);
