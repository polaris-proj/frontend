import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { useAppSelector } from '../../index';
import { IParam } from '../../../types/strategies/strategy';


interface IInitialState {
  data: Record<
    string,
    {
      loading: boolean;
      loaded: boolean;
      errorOnLoad: boolean;
      params: IParam[] | undefined;
    }
  >;
}

const initialState: IInitialState = {
  data: {},
};

const strategyParamsSlice = createSlice({
  name: 'strategy/params/list',
  initialState,
  reducers: {
    setData(
      state,
      action: PayloadAction<{
        id: string;
        data: IParam[];
      }>
    ) {
      state.data = {
        ...state.data,
        [action.payload.id]: {
          loading: false,
          loaded: true,
          errorOnLoad: false,
          params: action.payload.data,
        },
      };
    },
    setLoading(
      state,
      action: PayloadAction<{
        id: string;
        status: boolean;
      }>
    ) {
      state.data = {
        ...state.data,
        [action.payload.id]: {
          ...state.data[action.payload.id],
          loading: action.payload.status,
          loaded: false,
          errorOnLoad: false,
        },
      };
    },
    setError(
      state,
      action: PayloadAction<{
        id: string;
        error: boolean;
      }>
    ) {
      state.data = {
        ...state.data,
        [action.payload.id]: {
          ...state.data[action.payload.id],
          errorOnLoad: action.payload.error,
          loaded: false,
          loading: false,
        },
      };
    },
  },
});

export const { setData, setLoading, setError } = strategyParamsSlice.actions;
export const strategyParamsReducer = strategyParamsSlice.reducer;
export const useStrategyParamsSelector = () =>
  useAppSelector((state) => state.strategies.params);
