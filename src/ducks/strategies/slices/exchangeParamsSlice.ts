import {Exchange} from "~apiClients/DataTypes/Exchange";
import {TimeFrame} from "~apiClients/DataTypes/TimeFrame";


export interface IExchangedParamsItem {
  exchange: Exchange;
  sources: {
    pair: string;
    timeFrame: TimeFrame[];
  }[];
}

export const TimeFrameList = () => [
  {
    id: 60,
    name: "1m"
  },
  {
    id: 180,
    name: "3m",
  },
  {
    id: 300,
    name: "5m",
  },
  {
    id: 900,
    name: "15m",
  },
  {
    id: 1800,
    name: "30m",
  },
  {
    id: 3600,
    name: "1h",
  },
  {
    id: 14400,
    name: "4h",
  },
  {
    id: 43200,
    name: "12h",
  },
  {
    id: 86400,
    name: "1d",
  },
  {
    id: 259200,
    name: "3d",
  },
  {
    id: 604800,
    name: "1w",
  },
  {
    id: 2592000,
    name: "1M",
  },
];
