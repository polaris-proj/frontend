import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { useAppDispatch, useAppSelector } from '../../index';
import { IStrategy } from '../../../types/strategies/strategy';
import { fetchStrategiesList } from '../actions/strategiesList';
import type {StrategyWithParameters} from "~apiClients/DataTypes/StrategyWithParameters";

interface IInitialState {
  loading: boolean;
  loaded: boolean;
  errorOnLoad: boolean;
  data: StrategyWithParameters[];
}

const initialState: IInitialState = {
  loading: false,
  loaded: false,
  errorOnLoad: false,
  data: [],
};

const strategyListSlice = createSlice({
  name: 'strategy/list',
  initialState,
  reducers: {
    setLoading(state, action: PayloadAction<boolean>) {
      state.loading = action.payload;
    },
    setLoaded(
      state,
      action: PayloadAction<{
        loaded: boolean;
        errorOnLoad: boolean;
      }>
    ) {
      state.loaded = action.payload.loaded;
      state.loading = false;
      state.errorOnLoad = action.payload.errorOnLoad;
    },
    setData(state, action: PayloadAction<StrategyWithParameters[]>) {
      state.data = action.payload;
    },
    reset(state, PayloadAction) {
      state.loading = initialState.loading;
      state.loaded = initialState.loaded;
      state.errorOnLoad = initialState.errorOnLoad;
      state.data = initialState.data;
    }
  },
});

export const { setLoading, setLoaded, setData, reset } = strategyListSlice.actions;
export const strategyListReducer = strategyListSlice.reducer;
export const useStrategySelector = () =>
  useAppSelector((state) => state.strategies.list);
