import {
  dispose,
  IndicatorSeries,
  init,
  type KLineData,
  registerFigure,
  registerIndicator,
  registerOverlay
} from 'klinecharts'
import React, { useEffect } from 'react'
import { rectangleOverlayTemplate } from './KlineChartCustomization/Overlay/RectangleOverlay'
import { type Rectangle } from '~charts/Models/Accumulation'
import { type NoteViewModel } from '~/apiClients/DataTypes/NoteViewModel'
import { type OrderViewModel } from '~/apiClients/DataTypes/OrderViewModel'
import { type ChartViewModel } from '~/apiClients/DataTypes/ChartViewModel'
import { Icon } from '~/apiClients/DataTypes/Icon'
import { userCustomIndicatorFactory } from './KlineChartCustomization/Indicator/UserCustomIndicator'
import { type Point } from '~charts/Models/Dot'
import { orderAnnotation } from './KlineChartCustomization/Overlay/OrderAnnotation'
import { multilineText } from './KlineChartCustomization/Overlay/multilineText'
import {Dictionary} from "~utils/common/collectionsHelpers";

interface Annotation {
  name: string
  extendData: string
  points: Point[]
}

interface DrawKlinesProps {
  candles: KLineData[]
  rectangles?: Rectangle[]
  notes?: NoteViewModel[]
  orders?: OrderViewModel[]
  charts?: ChartViewModel[]
  chartsOptions?: Dictionary<boolean>
}

const GetFormattedText = (note: NoteViewModel): string => {
  return `${GetIcon(note)} ${note.text}`
}

const GetIcon = (note: NoteViewModel): string => {
  switch (note.icon) {
    case Icon.Default:
      return '❔'
    case Icon.Buy:
      return '✅'
    case Icon.Sell:
      return '❌'
  }
}

export const DrawKlines = ({ candles, rectangles, notes, orders, charts, chartsOptions }: DrawKlinesProps): React.JSX.Element => {
  console.log(chartsOptions)
  useEffect(() => {
    // initialize the chart
    const chart = init('overlay-k-line')
    chart?.setPriceVolumePrecision(3,1)
    if (chart == null) {
      return
    }

    chart.applyNewData(candles)

    if (rectangles != null) {
      for (const rect of rectangles) {
        const id = chart.createOverlay({
          name: rectangleOverlayTemplate.name,
          points: [
            {
              timestamp: rect.starttimestamp,
              value: rect.lowprice
            },
            {
              timestamp: rect.endtimestamp,
              value: rect.highprice
            }
          ]
        })
      }
    }

    if (notes != null) {
      const annotationsByNotes = notes.map<Annotation>(x => ({
        name: 'simpleAnnotation',
        extendData: GetFormattedText(x),
        points: [
          {
            timestamp: x.timeStamp,
            value: x.price
          }
        ]
      }))

      chart.createOverlay(annotationsByNotes)
    }

    if (orders != null) {
      registerFigure(multilineText)
      registerOverlay(orderAnnotation)
      const annotationsByOrder = orders.map<Annotation>(x => ({
        name: orderAnnotation.name,
        extendData: `Order: ${x.type}\n ${x.price}\n ${x.amount}`,
        points: [
          {
            timestamp: x.createdAt,
            value: x.price ?? 0
          }
        ]
      }))
      chart.createOverlay(annotationsByOrder)
    }

    if (charts != null && chartsOptions != null) {
      for (const userChart of charts) {
        const userCustomIndicator = userCustomIndicatorFactory(
          userChart.name,
          userChart.name,
          IndicatorSeries.Price,
          3,
          userChart.chart)

        registerIndicator(userCustomIndicator)
        if (chartsOptions[userChart.name + userChart.description]) {
          chart.createIndicator(userCustomIndicator, true, { id: 'candle_pane' })
        } else {
          chart.createIndicator(userCustomIndicator, true)
        }
      }
    }

    return () => {
      // destroy chart
      dispose('overlay-k-line')
    }
  }, [candles, rectangles, notes, orders, charts, chartsOptions])

  return <div id="overlay-k-line" className="k-line-chart-container" style={{ width: '100%', height: '100%' }}/>
}
