import { type Coordinate, type FigureTemplate, type RectAttrs, type TextAttrs, type TextStyle, utils } from 'klinecharts'
import { createFont } from '../Canvas'

export function getTextRect (attrs: TextAttrs, styles: Partial<TextStyle>): RectAttrs {
  const { size = 12, paddingLeft = 0, paddingTop = 0, paddingRight = 0, paddingBottom = 0, weight = 'normal', family } = styles
  const { x, y, text } = attrs
  const lines = text.split('\n')
  const longestLine = lines.reduce((a, b) => a.length > b.length ? a : b)
  const width = paddingLeft + utils.calcTextWidth(longestLine, size, weight, family) + paddingRight
  const lineAmount = lines.length
  const height = (paddingTop + 1.5 * size * lineAmount + paddingBottom)

  const startX = x - width / 2
  const startY = y - height

  return { x: startX, y: startY, width, height }
}

export function checkCoordinateOnText (coordinate: Coordinate, attrs: TextAttrs | TextAttrs[], styles: Partial<TextStyle>): boolean {
  let texts: TextAttrs[] = []
  texts = texts.concat(attrs)
  for (const item of texts) {
    const { x, y, width, height } = getTextRect(item, styles)
    if (
      coordinate.x >= x &&
            coordinate.x <= x + width &&
            coordinate.y >= y &&
            coordinate.y <= y + height
    ) {
      return true
    }
  }
  return false
}

export function drawText (ctx: CanvasRenderingContext2D, attrs: TextAttrs | TextAttrs[], styles: Partial<TextStyle>): void {
  let texts: TextAttrs[] = []
  texts = texts.concat(attrs)
  const {
    color = 'currentColor',
    size = 12,
    family,
    weight,
    paddingLeft = 0,
    paddingTop = 0,
    paddingRight = 0
  } = styles
  const lineHeight = 1.5 * size // Добавляем высоту строки
  const rect = getTextRect(texts[0], styles)

  utils.drawRect(ctx, rect, { ...styles, color: styles.backgroundColor })

  ctx.textAlign = 'left'
  ctx.textBaseline = 'top'
  ctx.font = createFont(size, weight, family)
  ctx.fillStyle = color
  texts.forEach((text) => {
    const lines = text.text.split('\n') // Разделяем текст на строки
    let lineY = rect.y + paddingTop
    lines.forEach((line) => {
      ctx.fillText(line, rect.x + paddingLeft, lineY, rect.width - paddingLeft - paddingRight)
      lineY += lineHeight // Переходим на следующую строку
    })
  })
}

export const multilineText: FigureTemplate<TextAttrs | TextAttrs[], Partial<TextStyle>> = {
  name: 'multilineText',
  checkEventOn: checkCoordinateOnText,
  draw: (ctx: CanvasRenderingContext2D, attrs: TextAttrs | TextAttrs[], styles: Partial<TextStyle>) => {
    drawText(ctx, attrs, styles)
  }
}
