import { OverlayMode, type OverlayTemplate, registerOverlay } from 'klinecharts'

export const rectangleOverlayTemplate: OverlayTemplate = {
  name: 'movableRectangle',
  totalStep: 3,
  needDefaultPointFigure: true,
  needDefaultXAxisFigure: true,
  needDefaultYAxisFigure: true,
  mode: OverlayMode.WeakMagnet,
  styles: {
    polygon: {
      color: 'rgba(22, 119, 255, 0.15)'
    }
  },
  createPointFigures: ({ coordinates }) => {
    if (coordinates.length > 1) {
      return [
        {
          type: 'polygon',
          attrs: {
            coordinates: [
              coordinates[0], { x: coordinates[1].x, y: coordinates[0].y },
              coordinates[1], { x: coordinates[0].x, y: coordinates[1].y }
            ]
          },
          styles: { style: 'stroke_fill' }
        }
      ]
    }
    return []
  }
}
registerOverlay(rectangleOverlayTemplate)
