import { LineType, utils, type OverlayTemplate } from 'klinecharts'
import { multilineText } from './multilineText'
export const orderAnnotation: OverlayTemplate = {
  name: 'orderAnnotation',
  totalStep: 2,
  styles: {
    line: { style: LineType.Dashed },
    multilineText: { color: 'white', backgroundColor: 'rgb(22, 119, 255)', paddingLeft: 5, paddingRight: 5, paddingTop: 5, size: 12 }
  },
  createPointFigures: ({ overlay, coordinates }) => {
    let text
    if (utils.isValid(overlay.extendData)) {
      if (!utils.isFunction(overlay.extendData)) {
        text = overlay.extendData ?? ''
      } else {
        text = overlay.extendData(overlay)
      }
    }
    const startX = coordinates[0].x
    const startY = coordinates[0].y - 6
    const lineEndY = startY - 50
    const arrowEndY = lineEndY - 5
    return [
      {
        type: 'line',
        attrs: { coordinates: [{ x: startX, y: startY }, { x: startX, y: lineEndY }] },
        ignoreEvent: true
      },
      {
        type: 'polygon',
        attrs: { coordinates: [{ x: startX, y: lineEndY }, { x: startX - 4, y: arrowEndY }, { x: startX + 4, y: arrowEndY }] },
        ignoreEvent: true
      },
      {
        type: multilineText.name,
        attrs: { x: startX, y: arrowEndY, text: text ?? '' },
        ignoreEvent: true
      }
    ]
  },
  onMouseEnter: (event): boolean => {
    console.log('onMouseEnter', event)
    return true
  },
  onMouseLeave: (event): boolean => {
    console.log('onMouseLeave', event)
    return true
  }

}
