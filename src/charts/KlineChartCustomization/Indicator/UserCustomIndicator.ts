import { type IndicatorSeries, type IndicatorTemplate, type KLineData } from 'klinecharts'
import type { Point } from '~apiClients/DataTypes/Point'

interface UserCustomIndicator {
  value?: number
}

export const userCustomIndicatorFactory = (name: string, shortName: string, series: IndicatorSeries, precision: number, userData: Point[]): IndicatorTemplate<UserCustomIndicator> => {
  return {
    name,
    shortName,
    series,
    precision,
    figures: [
      {
        key: 'value',
        title: 'UserCustom: ',
        type: 'line'
      }
    ],
    calc: (dataList: KLineData[]) => {
      return dataList.map((kLineData: KLineData) => {
        for (const data of userData) {
          if (data.timeStamp === kLineData.timestamp) {
            const avp: UserCustomIndicator = {
              value: data.value
            }
            return avp
          }
        }
        return {  }
      })
    }
  }
}
