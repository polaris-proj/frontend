import { List } from 'antd'
import React, {useEffect} from 'react'
import { type Pair } from '~apiClients/DataTypes/Pair'
import VirtualList from 'rc-virtual-list';
export const ScrolledList = ({ data, onClickCallback }: { data: Pair[], onClickCallback: (x: Pair) => void }): JSX.Element => {
  const [selectedIndex, setSelectedIndex] = React.useState(1)

  const handleListItemClick = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>,
    index: number
  ): void => {
    setSelectedIndex(index)
  }

  // useEffect(() => {
  //   //appendData();
  // }, []);

  // const onScroll = (e) => {
  //   // Refer to: https://developer.mozilla.org/en-US/docs/Web/API/Element/scrollHeight#problems_and_solutions
  //   if (Math.abs(e.currentTarget.scrollHeight - e.currentTarget.scrollTop - 100%) <= 1) {
  //    // appendData();
  //   }
  // };

  return (
      <div style={{ maxHeight: '100%', width: '100%', overflow: 'hidden' }}>
          <List>
            <VirtualList
              data={data}
              height={100}
              itemHeight={47}
              itemKey="id"
            >
              {(item, index) => (
                <List.Item
                  key={`${item.first}${item.second}`}
                  onClick={(event) => {
                    handleListItemClick(event, index)
                    onClickCallback(item)
                }}>
                  <div style={{textAlign:'center'}}>
                    {`${item.first}${item.second}`}
                  </div>
                </List.Item>
              )}
            </VirtualList>
          </List>
      </div>
  )
}
