import React, { useEffect, useState } from 'react'
import { type KLineData } from 'klinecharts'
import { DrawKlines } from './DrawKlines'
import { useExchangeDataApi, useStrategyApi } from '~/apiClients/hooks/useApi'
import { type Exchange } from '~/apiClients/DataTypes/Exchange'
import { type Pair } from '~/apiClients/DataTypes/Pair'
import { TimeFrame } from '~/apiClients/DataTypes/TimeFrame'
import { ScrolledList } from './Components/ScrolledList'
import type { ExchangeSources } from '~/apiClients/DataTypes/ExchangeSources'
import { type Candle } from '~/apiClients/DataTypes/Candle'
import { type NoteViewModel } from '~/apiClients/DataTypes/NoteViewModel'
import { type ChartViewModel } from '~/apiClients/DataTypes/ChartViewModel'
import type { OrderViewModel } from '~/apiClients/DataTypes/OrderViewModel'
import {Dictionary, uniqueArray} from "~utils/common/collectionsHelpers";
import {Button, Checkbox, Dropdown, Select, Space} from "antd";
import type {MenuProps} from "antd";
import { DownOutlined } from '@ant-design/icons'

const MapCandle = (c: Candle): KLineData => ({
  timestamp: c.timeStamp,
  open: c.open,
  high: c.high,
  low: c.low,
  close: c.close,
  volume: c.volume
})

const DistinctPairs = (pairs: Pair[]): Pair[] => {
  const map = new Map<string, Pair>()
  pairs.forEach(p => {
    map.set(`${p.first}${p.second}`, p)
  })
  return Array.from(map.values())
}

const ComparePairs = (a: Nullable<Pair>, b: Nullable<Pair>): boolean => {
  if (a == null || b == null) {
    return false
  }
  return a.first === b.first && a.second === b.second
}

export interface ReportViewerProps{
  runId: string;
  startTime: Date|string;
  endTime: Date|string;
}

export const ReportViewer = ({ runId, startTime, endTime }: ReportViewerProps): React.JSX.Element => {
  const [exchange, setExchange] = useState<Nullable<Exchange>>(null)
  const [pair, setPair] = useState<Nullable<Pair>>(null)
  const [timeFrame, setTimeFrame] = useState<TimeFrame>(TimeFrame.Unknown)

  const [candles, setCandles] = useState<KLineData[]>([])
  const [startDate, setStartDate] = useState(startTime)
  const [endDate, setEndDate] = useState(endTime)
  const [source, setSource] = useState<Nullable<ExchangeSources[]>>(null)
  const [allOrders, setAllOrders] = useState<OrderViewModel[]>([])
  const [notes, setNotes] = useState<NoteViewModel[]>([])
  const [charts, setCharts] = useState<ChartViewModel[]>([])
  const [chartsShowedOnCandles, setChartsShowedOnCandles] = useState<string[]>([])
  const [chartsSettings, setChartsSettings] = useState<Dictionary<boolean>>({})

  useStrategyApi(c => {
    c.getTestStrategyParameters(runId)
      .then(r => {
        setSource(r.allowedSource)
        if (r.allowedSource.length > 0) {
          const first = r.allowedSource[0]
          setExchange(first.exchange)
        } else {
          console.error('No allowed source')
        }
      })
      .catch(e => { console.error(e) })
  }, [runId])

  useStrategyApi(c => {
    if (exchange != null && pair != null && timeFrame !== TimeFrame.Unknown) {
      c.getResultOfExperiment(runId, exchange, pair.first, pair.second, timeFrame, startTime, endTime)
        .then(r => {
          setNotes(r.notes)
          setAllOrders(r.orders)
          setCharts(r.charts)
        })
        .catch(e => { console.error(e) })
    }
  }, [runId, exchange, pair, timeFrame])

  useExchangeDataApi(c => {
    if (exchange != null && pair != null && timeFrame !== TimeFrame.Unknown) {
      c.getCandles(
        exchange,
        pair.first,
        pair.second,
        timeFrame,
        startDate,
        endDate
      ).then(r => { setCandles(r.map(MapCandle)) })
        .catch(e => { console.error(e) })
    }
  }, [exchange, pair, timeFrame, startDate, endDate])

  useEffect(() => {
    setChartsSettings(Object.fromEntries(chartsShowedOnCandles.map(x => [x, true])))
  }, [chartsShowedOnCandles])

  if (source == null || exchange == null) {
    return <div>Loading...</div>
  }

  const items : MenuProps['items'] = charts.map(x => ({
    key: x.name + x.description,
    label: (<>
      <Checkbox
          checked={chartsShowedOnCandles.includes(x.name + x.description)}
          tabIndex={-1}
          onClick={(_) => {
            const key = x.name + x.description
            if (chartsShowedOnCandles.includes(key)) {
              setChartsShowedOnCandles(chartsShowedOnCandles.filter(o => o !== key))
            } else {
              setChartsShowedOnCandles([...chartsShowedOnCandles, key])
            }
          }}
        />
    Show {x.name} on candles chart
      </>),
  }))

  return (
        <div style={{ flexDirection: 'row',flexWrap: 'wrap',display: 'flex', justifyContent: 'space-between'}}>
            <div style={{ display: 'flex', flexDirection: 'column', width: '90%' }}>
              <div style={{ height: '8%', minWidth: 120, display: 'flex', flexDirection: 'row' }}>
                        <Select
                            placeholder="TimeFrame"
                            value={timeFrame}
                            onChange={ setTimeFrame }
                            options={uniqueArray(source.filter(x => x.exchange === exchange)
                              .flatMap(x => x.pairTimeFrame)
                              .filter(y => ComparePairs(y.pair, pair ?? { first: '', second: '' }))
                              .map(y => y.timeFrame)
                            ).map(y => ({ value:y, label:y}))
                        }
                        />
                        {charts.length > 0 &&
                            <Space wrap>
                                    <Dropdown menu={{items}}>
                                        <a onClick={(e) => e.preventDefault()}>
                                            <Space>
                                                <Button>
                                                    Indicator settings
                                                    <DownOutlined/>
                                                </Button>
                                            </Space>
                                        </a>
                                    </Dropdown>
                            </Space>
                        }
                    </div>
              <div style={{ height: '92%', minHeight: 500 }}>
                    <DrawKlines candles={candles} orders={allOrders} notes={notes} charts={charts} chartsOptions={chartsSettings}/>
                </div>
            </div>
            <div>
                <div style={{height: '6%', display: 'flex'}}>
                    <div style={{ minWidth: 100, display: 'block' }}>
                        <Select
                          placeholder="Exchange"
                            value={exchange}
                            onChange={(e) => {setExchange(e)}}
                            options={uniqueArray(source.map(x => x.exchange))
                              .map((x) => ({value:x,label:x}))}
                        />
                    </div>
                </div>
                <div style={{ width: '10%', minWidth: '120px', height: '30vh' }}>
                    <ScrolledList data={DistinctPairs(
                      source.filter(x => x.exchange === exchange)
                        .flatMap(x => x.pairTimeFrame.map(y => y.pair))
                    )}
                    onClickCallback={setPair}/>
                </div>
            </div>
        </div>
  )
}
