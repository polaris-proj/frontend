export interface Dot {
  timeStamp: number
  price: number
}

export interface Point {
  timestamp: number
  value: number
}
