import { test, expect } from '@playwright/test';

test('redirect to login page', async ({ page }) => {
  await page.goto('/');

  await expect(page).toHaveTitle(/Login/);
});
